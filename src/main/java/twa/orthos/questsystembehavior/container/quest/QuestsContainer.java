
package twa.orthos.questsystembehavior.container.quest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import lombok.Getter;
import lombok.Setter;
import twa.orthos.questsystembehavior.model.Quest;
import twa.orthos.questsystembehavior.model.QuestModel;

/**
 * Container class to store created quests. The container has the purpose to
 * synchronise the internal quest list with the quest name list on screen.
 * 
 * @author totalwarANGEL
 *
 */
@Getter
@Setter
public class QuestsContainer
{
	private static QuestsContainer instance;

	protected List<Quest> questList;

	/**
	 * Private constructor.
	 */
	private QuestsContainer()
	{
		questList = new ArrayList<>();
	}

	/**
	 * Returns the singleton instance of the quest container.
	 * 
	 * @return Instance
	 */
	public static QuestsContainer getInstance()
	{
		if (instance == null)
		{
			instance = new QuestsContainer();
		}
		return instance;
	}

	/**
	 * Sorts the list of quests alphabetically from A to Z.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void sort()
	{
		final Comparator c = (a, b) -> ((Quest) a).getName().compareTo(((Quest) b).getName());
		questList.sort(c);
	}

	/**
	 * Returns the quest with the index.
	 * 
	 * @param idx Quest index
	 * @return Quest
	 */
	public Quest getQuest(final int idx)
	{
		if (idx > -1 && questList.size() > idx)
		{
			return questList.get(idx);
		}
		return null;
	}

	/**
	 * Returns the quest with the name from the internal list.
	 * 
	 * @param name Quest name
	 * @return Quest
	 */
	public Quest getQuestByName(final String name)
	{
		for (int i = 0; i < questList.size(); i++)
		{
			if (questList.get(i).getName().equals(name))
			{
				return questList.get(i);
			}
		}
		return null;
	}

	/**
	 * Returns a vector of quest names as content for the quest list in the
	 * interface.
	 * 
	 * @return Quest names
	 */
	public Vector<String> getQuestNames()
	{
		final Vector<String> namesList = new Vector<>();
		for (int i = 0; i < questList.size(); i++)
		{
			namesList.add(questList.get(i).getName());
		}
		return namesList;
	}

	/**
	 * Removes a quest from the internal list.
	 * 
	 * @param idx Quest index
	 */
	public void removeQuest(final int idx)
	{
		if (idx > -1 && questList.size() > idx)
		{
			questList.remove(idx);
		}
	}

	/**
	 * Creates a new quest and add it to the internal list.
	 * 
	 * @param name Quest name
	 * @param receiver Quest receiver
	 * @param time Completion time
	 * @param type Quest type
	 * @param title Displayed title
	 * @param text Displayed text
	 * @param visible Quest is visible
	 */
	public void createQuest(
		final String name, final String receiver, final String time, final String type, final String title,
		final String text, final boolean visible
	)
	{
		if (getQuestByName(name) != null)
		{
			return;
		}

		final QuestModel newQuest = new QuestModel();
		newQuest.setName(name);
		newQuest.setReceiver(receiver);
		newQuest.setTime(time);
		newQuest.setQuestType(type);
		newQuest.setQuestTitle(title);
		newQuest.setQuestText(text);
		newQuest.setVisible(visible);
		newQuest.setBehavior(new ArrayList<List<String>>());
		questList.add(newQuest);
	}
}
