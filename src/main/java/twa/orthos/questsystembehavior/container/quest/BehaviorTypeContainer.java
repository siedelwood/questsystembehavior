
package twa.orthos.questsystembehavior.container.quest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import twa.orthos.questsystembehavior.model.Behavior;
import twa.orthos.questsystembehavior.model.Parameter;
import twa.orthos.questsystembehavior.model.ParameterModel;

/**
 * Container class to store the known behavior templates. The container has the
 * purpose to synchronize the internal behavior list with the behavior name list
 * on screen.
 * 
 * @author totalwarANGEL
 *
 */
public class BehaviorTypeContainer
{
	private static BehaviorTypeContainer instance;

	protected List<Behavior> behaviorList;

	/**
	 * Private constructor.
	 */
	private BehaviorTypeContainer()
	{
		behaviorList = new ArrayList<>();
	}

	/**
	 * Returns the singleton instance of the behavior container.
	 * 
	 * @return Instance
	 */
	public static BehaviorTypeContainer getInstance()
	{
		if (instance == null)
		{
			instance = new BehaviorTypeContainer();
		}
		return instance;
	}
	
	/**
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void sort() {
		final Comparator c = (a, b) -> ((Behavior) a).getName().compareTo(((Behavior) b).getName());
		behaviorList.sort(c);
	}

	/**
	 * Sets the list of known behavior templates.
	 * 
	 * @param behaviors Behavior templates
	 */
	public void setBehaviors(final List<Behavior> behaviors)
	{
		behaviorList = behaviors;
	}

	/**
	 * Returns the list of known behavior templates.
	 * 
	 * @return Behavior list
	 */
	public List<Behavior> getBehaviors()
	{
		return behaviorList;
	}

	/**
	 * Returns the behavior template with the behavior name.
	 * 
	 * @param name Behavior name
	 * @return Behavior template
	 */
	public Behavior getBehaviorByName(final String name)
	{
		for (int i = 0; i < behaviorList.size(); i++)
		{
			if (behaviorList.get(i).getName().equals(name))
			{
				return behaviorList.get(i);
			}
		}
		return null;
	}

	/**
	 * Returns the parameter of the behavior.
	 * 
	 * @param name Behavior name
	 * @return Parameter list
	 */
	@SuppressWarnings("unchecked")
	public List<Parameter> getBehaviorParameter(final String name)
	{
		final List<Parameter> params = new ArrayList<>();
		final Behavior behavior = getBehaviorByName(name);
		for (int i = 0; i < behavior.getFields().size(); i++)
		{
			final ParameterModel parameter = new ParameterModel();
			parameter.setName((String) behavior.getFields().get(i).get(1));
			parameter.setType((String) behavior.getFields().get(i).get(0));
			parameter.setValues((List<String>) behavior.getFields().get(i).get(2));
			params.add(parameter);
		}
		return params;
	}
}
