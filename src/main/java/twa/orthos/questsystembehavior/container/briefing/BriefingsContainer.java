
package twa.orthos.questsystembehavior.container.briefing;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import twa.orthos.questsystembehavior.model.Briefing;

/**
 * Container class to store created briefings. The container has the purpose to
 * synchronise the internal briefing list with the briefing tree on screen.
 * 
 * @author totalwarANGEL
 */
@Getter
@Setter
public class BriefingsContainer
{
	private static BriefingsContainer instance;

	protected List<Briefing> briefingList;

	/**
	 * Private constructor.
	 */
	private BriefingsContainer()
	{
		briefingList = new ArrayList<>();
	}

	/**
	 * Returns the singleton instance of the quest container.
	 * 
	 * @return Instance
	 */
	public static BriefingsContainer getInstance()
	{
		if (instance == null)
		{
			instance = new BriefingsContainer();
		}
		return instance;
	}

	/**
	 * Sorts the list of briefings alphabetically from A to Z.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void sort()
	{
		final Comparator c = (a, b) -> ((Briefing) a).getName().compareTo(((Briefing) b).getName());
		briefingList.sort(c);
	}

	/**
	 * Adds a Briefing to the briefing list.
	 * 
	 * @param briefing Briefing to add
	 */
	public void addBriefing(final Briefing briefing)
	{
		briefingList.add(briefing);
	}

	/**
	 * Adds a Briefing to the briefing list at the desired index.
	 * 
	 * @param briefing Briefing to add
	 * @param index Position
	 */
	public void addBriefing(final Briefing briefing, final int index)
	{
		briefingList.add(index, briefing);
	}
	
	/**
     * 
     * 
     * @param briefing Briefing to add
     * @param index Position
     */
    public void replaceBriefing(final Briefing briefing, final int index)
    {
        briefingList.remove(index);
        briefingList.add(index, briefing);
    }

	/**
	 * Removes the Briefing at the idex from the briefing list.
	 * 
	 * @param index Index position
	 */
	public void removeBriefing(final int index)
	{
		briefingList.remove(index);
	}

	/**
	 * Returns the briefing at the index in the briefing list.
	 * 
	 * @param index Index position
	 * @return Briefing
	 */
	public Briefing getBriefing(final int index)
	{
		if (index > (briefingList.size() - 1))
		{
			return null;
		}
		return briefingList.get(index);
	}

	/**
	 * Returns the index of the briefing.
	 * 
	 * @param briefing Briefing to find
	 * @return Index
	 */
	public int getIndexOfBriefing(final Briefing briefing)
	{
		for (int i = 0; i < briefingList.size(); i++)
		{
			if (briefing.equals(briefingList.get(i)))
			{
				return i;
			}
		}
		return -1;
	}
}
