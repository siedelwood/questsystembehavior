
package twa.orthos.questsystembehavior.service;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import twa.orthos.questsystembehavior.model.FileChooserResult;

/**
 * This service holds the JFileChooser instances used for selecting load and
 * save directories.
 * 
 * @author totalwarANGEL
 */
@Getter
@Setter
@NoArgsConstructor
public class FileChooserService
{

    protected JFileChooser loadMapChooser;

    protected JFileChooser saveMapChooser;

    protected JFileChooser loadDataChooser;

    protected JFileChooser saveDataChooser;

    /**
     * Creates the file chooser for selectiong settlers maps.
     * @param baseDirectory
     */
    protected void createLoadMapChooser(final String baseDirectory)
    {
        if (loadMapChooser == null)
        {
            loadMapChooser = new JFileChooser();
            loadMapChooser.setAcceptAllFileFilterUsed(false);
            loadMapChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            loadMapChooser.addChoosableFileFilter(new FileNameExtensionFilter("Kartenarchiv", "s5x"));
            loadMapChooser.setSelectedFile(new File(baseDirectory));
            loadMapChooser.setMultiSelectionEnabled(false);
        }
    }

    /**
     * Opens the load dialog for settlers maps. If the dialog is opened for the first
     * time the base directory is set to "user.home".
     * @return File chooser result
     */
    public FileChooserResult openLoadMapChooser()
    {
        final FileChooserResult result = new FileChooserResult(false, null);
        createLoadMapChooser(System.getProperty("user.home"));
        if (loadMapChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
        {
            final File file = loadMapChooser.getSelectedFile();
            result.setApproved(true);
            result.setSelectedFile(file);
        }
        return result;
    }

    /**
     * FIXME: Not implemented
     * @param baseDirectory Base directory
     */
    protected void createSaveMapChooser(final String baseDirectory)
    {
    }

    /**
     * FIXME: Not implemented
     * @return File chooser result
     */
    public FileChooserResult openSaveMapChooser()
    {
        final FileChooserResult result = new FileChooserResult(false, null);
        return result;
    }

    /**
     * Creates the file chooser for loading mission data.
     * @param baseDirectory Base directory
     */
    protected void createLoadDataChooser(final String baseDirectory)
    {
        if (loadDataChooser == null)
        {
            loadDataChooser = new JFileChooser();
            loadDataChooser.setAcceptAllFileFilterUsed(false);
            loadDataChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            loadDataChooser.addChoosableFileFilter(new FileNameExtensionFilter("Kartenkonfiguration", "s5m"));
            loadDataChooser.setSelectedFile(new File(baseDirectory));
            loadDataChooser.setMultiSelectionEnabled(false);
        }
    }

    /**
     * Opens the load dialog for mission data. If the dialog is opened for the first
     * time the base directory is set to "user.home".
     * 
     * @return File chooser result
     */
    public FileChooserResult openLoadDataChooser()
    {
        final FileChooserResult result = new FileChooserResult(false, null);
        createLoadDataChooser(System.getProperty("user.home"));
        if (loadDataChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
        {
            final File file = loadDataChooser.getSelectedFile();
            result.setApproved(true);
            result.setSelectedFile(file);
        }
        return result;
    }

    /**
     * Creates the file chooser for saving mission data if it does not exist.
     * 
     * @param baseDirectory Base directory
     */
    protected void createSaveDataChooser(final String baseDirectory)
    {
        if (saveDataChooser == null)
        {
            saveDataChooser = new JFileChooser();
            saveDataChooser.setAcceptAllFileFilterUsed(false);
            saveDataChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            saveDataChooser.addChoosableFileFilter(new FileNameExtensionFilter("Kartenkonfiguration", "s5m"));
            saveDataChooser.setSelectedFile(new File(baseDirectory));
            saveDataChooser.setMultiSelectionEnabled(false);
        }
    }

    /**
     * Opens the save dialog for mission data. If the dialog is opened for the first
     * time the base directory is set to "user.home".
     * 
     * @return File chooser result
     */
    public FileChooserResult openSaveDataChooser()
    {
        final FileChooserResult result = new FileChooserResult(false, null);
        createSaveDataChooser(System.getProperty("user.home"));
        if (saveDataChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
        {
            File file = saveDataChooser.getSelectedFile();
            if (!file.getAbsolutePath().endsWith(".s5m"))
            {
                file = new File(file.getAbsolutePath() + ".s5m");
            }

            result.setApproved(true);
            result.setSelectedFile(file);
        }
        return result;
    }
}
