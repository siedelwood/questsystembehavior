
package twa.orthos.questsystembehavior.service.lua;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import twa.orthos.questsystembehavior.model.Behavior;
import twa.orthos.questsystembehavior.model.Quest;

/**
 * The lua quest builder converts a list of quests to a lua table.
 * 
 * @author totalwarANGEL
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LuaQuestBuilder implements QuestBuilder
{

	private List<Behavior> behaviorTypes;
	
	private List<Quest> questList;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String transformQuests()
	{
		final StringBuilder builder = new StringBuilder();
		parseQuests(questList, builder);
		return builder.toString();
	}

	/**
	 * Parses a list of quests to a lua string.
	 * 
	 * @param quests Quest list
	 * @param builder String builder
	 */
	protected void parseQuests(final List<Quest> quests, final StringBuilder builder)
	{
		builder.append("{").append("\n");
		for (int i = 0; i < quests.size(); i++)
		{
			parseSingleQuest(quests.get(i), builder);
		}
		builder.append("    }");
	}

	/**
	 * Parses a single quest to a lua string.
	 * 
	 * @param quests Quest list
	 * @param builder String builder
	 */
	protected void parseSingleQuest(final Quest quest, final StringBuilder builder)
	{
		builder.append("        {");

		builder.append("Name = \"").append(quest.getName()).append("\",");
		builder.append("Time = ").append(quest.getTime()).append(",");
		builder.append("Receiver = ").append(quest.getReceiver()).append(",");
		if (quest.isVisible())
		{
			builder.append("Description = {");
			builder.append("Info = 1,");
			builder.append("Title = \"").append(quest.getQuestTitle()).append("\",");
			builder.append("Text = \"").append(quest.getQuestText()).append("\",");
			builder.append("Type = ").append(quest.getQuestType()).append(",");
			builder.append("},");
		}
		parseBehavior(quest.getBehavior(), builder);

		builder.append("},").append("\n");
	}

	/**
	 * Parses a list of quest behaviors to a lua string.
	 * 
	 * @param quests Behavior list
	 * @param builder String builder
	 */
	protected void parseBehavior(final List<List<String>> behaviors, final StringBuilder builder)
	{
		for (int i = 0; i < behaviors.size(); i++)
		{
			parseSingleBehavior(behaviors.get(i), builder);
		}
	}

	/**
	 * Parses a single behavior to a lua string.
	 * 
	 * @param quests Behavior
	 * @param builder String builder
	 */
	protected void parseSingleBehavior(final List<String> behavior, final StringBuilder builder)
	{
		builder.append("{").append("\"").append(behavior.get(0)).append("\",");

		final List<String> parameterTypes = getBehaviorParameter(behavior.get(0));
		for (int i = 1; i < behavior.size(); i++)
		{
			String arg = behavior.get(i);
			if (parameterTypes.get(i - 1).equals("StringInput") || parameterTypes.get(i - 1).equals("StringSelect"))
			{
				arg = "\"" + arg + "\"";
			}
			builder.append(arg).append(",");
		}
		builder.append("},");
	}

	/**
	 * Returns the parameter types of the behavior
	 * 
	 * @param behaviorName Name of behavior
	 * @return Parameter types
	 */
	protected List<String> getBehaviorParameter(final String behaviorName)
	{
		final List<String> parameter = new ArrayList<>();
		for (int i = 0; i < behaviorTypes.size(); i++)
		{
			if (behaviorTypes.get(i).getName().equals(behaviorName))
			{
				final List<List<Object>> parameterData = behaviorTypes.get(i).getFields();
				for (int j = 0; j < parameterData.size(); j++)
				{
					parameter.add((String) parameterData.get(j).get(0));
				}
			}
		}
		return parameter;
	}
}
