
package twa.orthos.questsystembehavior.service.lua;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import twa.orthos.questsystembehavior.model.DiplomacyState;
import twa.orthos.questsystembehavior.model.MapSettings;
import twa.orthos.questsystembehavior.model.PlayerColor;

/**
 * This class replaces the tokens inside the internal and external map script
 * with the data from the assistant.
 * 
 * @author totalwarANGEL
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MapScriptBuilder
{
	private QuestBuilder questBuilder;

	private BriefingBuilder briefingBuilder;

	private MapSettings mapSettings;

	/**
	 * Loads a template and replaces all tokens with the appropriate contents.
	 * 
	 * @param filepath Template to load
	 * @return Lua string
	 */
	public String replaceTokensInMapscript(final String filepath)
	{
		List<String> lines;
		try
		{
			lines = Files.readAllLines(Paths.get("lua/mainmapscript.lua"), Charset.forName("UTF-8"));
			final StringBuilder mapScript = new StringBuilder();
			for (int i = 0; i < lines.size(); i++)
			{
				String lineToAppend = lines.get(i);
				lineToAppend = replaceQuestToken(lineToAppend);
				lineToAppend = replaceBriefingToken(lineToAppend);
				lineToAppend = replaceDebugToken(lineToAppend);
				lineToAppend = replaceWeatherToken(lineToAppend);
				lineToAppend = replaceResourceToken(lineToAppend);
				lineToAppend = replaceDiplomacyToken(lineToAppend);
				lineToAppend = replaceColorToken(lineToAppend);
				mapScript.append(lineToAppend).append("\n");
			}
			return mapScript.toString();
		}
		catch (final IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Replaces the tokens inside the public map script.
	 * 
	 * @param filepath Template path
	 * @return Lua string
	 */
	public String replaceTokensInPublicScript(final String filepath)
	{
		List<String> lines;
		try
		{
			lines = Files.readAllLines(Paths.get("lua/mapscript.lua"), Charset.forName("UTF-8"));
			final StringBuilder mapScript = new StringBuilder();
			for (int i = 0; i < lines.size(); i++)
			{
				String lineToAppend = lines.get(i);

				if (lineToAppend.contains("QUEST_ASSISTENT_META_MAPNAME"))
				{
					lineToAppend = lineToAppend.replaceFirst(
						"QUEST_ASSISTENT_META_MAPNAME", String.format("%1$-59s", mapSettings.getMetaData()[0])
					);
				}

				if (lineToAppend.contains("QUEST_ASSISTENT_META_AUTHOR"))
				{
					lineToAppend = lineToAppend.replaceFirst(
						"QUEST_ASSISTENT_META_AUTHOR", String.format("%1$-57s", mapSettings.getMetaData()[1])
					);
				}

				if (lineToAppend.contains("QUEST_ASSISTENT_META_VERSION"))
				{
					lineToAppend = lineToAppend.replaceFirst(
						"QUEST_ASSISTENT_META_VERSION", String.format("%1$-57s", mapSettings.getMetaData()[2])
					);
				}

				mapScript.append(lineToAppend).append("\n");
			}
			return mapScript.toString();
		}
		catch (final IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Replaces the quest token with the generated briefings.
	 * 
	 * @param lineToAppend Line token is in
	 * @return Lua string
	 */
	private String replaceQuestToken(String lineToAppend)
	{
		if (lineToAppend.contains("QUEST_ASSISTENT_GENERATED_QUESTS"))
		{
			lineToAppend = lineToAppend
				.replaceFirst("QUEST_ASSISTENT_GENERATED_QUESTS", questBuilder.transformQuests());
		}
		return lineToAppend;
	}

	/**
	 * Replaces the briefing token with the generated quests.
	 * 
	 * @param lineToAppend Line token is in
	 * @return Lua string
	 */
	private String replaceBriefingToken(String lineToAppend)
	{
		if (lineToAppend.contains("QUEST_ASSISTENT_GENERATED_BRIEFINGS"))
		{
			lineToAppend = lineToAppend
				.replaceFirst("QUEST_ASSISTENT_GENERATED_BRIEFINGS", briefingBuilder.transformBriefings());
		}
		return lineToAppend;
	}

	/**
	 * Replaces the debug token with the configured debug options.
	 * 
	 * @param lineToAppend Line token is in
	 * @return Lua string
	 */
	private String replaceDebugToken(String lineToAppend)
	{
		if (lineToAppend.contains("QUEST_ASSISTENT_SETTINGS_DEBUG_OPTIONS"))
		{
			lineToAppend = lineToAppend.replaceFirst(
				"QUEST_ASSISTENT_SETTINGS_DEBUG_OPTIONS",
				String.format(
					"{%b,%b,%b,%b}", mapSettings.getDebugOptions()[0], mapSettings.getDebugOptions()[1],
					mapSettings.getDebugOptions()[2], mapSettings.getDebugOptions()[3]
				)
			);
		}
		return lineToAppend;
	}

	/**
	 * Replaces the weather token with the configured debug options.
	 * 
	 * @param lineToAppend Line token is in
	 * @return Lua string
	 */
	private String replaceWeatherToken(String lineToAppend)
	{
		if (lineToAppend.contains("QUEST_ASSISTENT_SETTINGS_WEATHER_SET"))
		{
			lineToAppend = lineToAppend
				.replaceFirst("QUEST_ASSISTENT_SETTINGS_WEATHER_SET", mapSettings.getMapWeather().getSet() + "()");
		}
		return lineToAppend;
	}

	/**
	 * Replaces the resource token with the configured starting resources.
	 * 
	 * @param lineToAppend Line token is in
	 * @return Lua string
	 */
	private String replaceResourceToken(String lineToAppend)
	{
		if (lineToAppend.contains("QUEST_ASSISTENT_SETTINGS_RESOURCES"))
		{
			lineToAppend = lineToAppend.replaceFirst(
				"QUEST_ASSISTENT_SETTINGS_RESOURCES",
				String.format(
					"{%d,%d,%d,%d,%d,%d}", mapSettings.getStartResources()[0], mapSettings.getStartResources()[1],
					mapSettings.getStartResources()[2], mapSettings.getStartResources()[3],
					mapSettings.getStartResources()[4], mapSettings.getStartResources()[5]
				)
			);
		}
		return lineToAppend;
	}

	/**
	 * Replaces the diplomacy token with the configured diplomatic settings.
	 * 
	 * @param lineToAppend Line token is in
	 * @return Lua string
	 */
	private String replaceDiplomacyToken(String lineToAppend)
	{
		if (lineToAppend.contains("QUEST_ASSISTENT_SETTINGS_DIPLOMACY"))
		{
			final List<DiplomacyState> diplomacyStates = mapSettings.getDiplomacyStates();
			String diplomacy = "";
			for (int i = 0; i < 8; i++)
			{
				if (i > 0)
				{
					diplomacy += ",";
				}
				diplomacy += "[" + (i + 1) + "]={\"" + diplomacyStates.get(i).getName() + "\",";
				diplomacy += "Diplomacy." + diplomacyStates.get(i).getDiplomacy() + "}";
			}

			lineToAppend = lineToAppend.replaceFirst("QUEST_ASSISTENT_SETTINGS_DIPLOMACY", "{" + diplomacy + "}");
		}
		return lineToAppend;
	}

	/**
	 * Replaces the color with the configured player colors.
	 * 
	 * @param lineToAppend Line token is in
	 * @return Lua string
	 */
	private String replaceColorToken(String lineToAppend)
	{
		if (lineToAppend.contains("QUEST_ASSISTENT_SETTINGS_PLAYER_COLOR"))
		{
			final List<PlayerColor> playerColors = mapSettings.getPlayerColors();
			String colors = "";
			for (int i = 0; i < 8; i++)
			{
				if (i > 0)
				{
					colors += ",";
				}
				colors += "[" + (i + 1) + "]=" + playerColors.get(i).getName();
			}

			lineToAppend = lineToAppend.replaceFirst("QUEST_ASSISTENT_SETTINGS_PLAYER_COLOR", "{" + colors + "}");
		}
		return lineToAppend;
	}
}
