
package twa.orthos.questsystembehavior.service.lua;

import java.util.List;
import twa.orthos.questsystembehavior.model.Briefing;

/**
 * Interface for a briefing builder.
 * 
 * @author totalwarANGEL
 *
 */
public interface BriefingBuilder
{
	/**
	 * Transforms the briefings to a lua string.
	 * 
	 * @return Lua string
	 */
	public String transformBriefings();

	/**
	 * Transforms a single briefing to a lua string.
	 * 
	 * @param briefing Briefing
	 * @return Lua string
	 */
	public String transformSingleBriefing(Briefing briefing);

	/**
	 * Sets the list of briefings that should be transformed.
	 * 
	 * @param briefingList List of Briefings
	 */
	public void setBriefings(List<Briefing> briefingList);
}
