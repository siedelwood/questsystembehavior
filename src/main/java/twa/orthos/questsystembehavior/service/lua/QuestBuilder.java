
package twa.orthos.questsystembehavior.service.lua;

import java.util.List;
import twa.orthos.questsystembehavior.model.Behavior;
import twa.orthos.questsystembehavior.model.Quest;

/**
 * Interface for lua quest builders.
 * 
 * @author totalwarANGEL
 *
 */
public interface QuestBuilder
{
	/**
	 * Transforms all quests to lua code.
	 * 
	 * @return Lua string
	 */
	public String transformQuests();

	/**
	 * Returns the list of quests.
	 * 
	 * @return Quest list
	 */
	public List<Quest> getQuestList();

	/**
	 * Returns the list of behaviors.
	 * 
	 * @return Behavior list
	 */
	public List<Behavior> getBehaviorTypes();

	/**
	 * Sets the list of quests.
	 * 
	 * @param questList Quest list
	 */
	public void setQuestList(List<Quest> questList);

	/**
	 * Sets the list of behavior.
	 * 
	 * @param behaviors Behavior list
	 */
	public void setBehaviorTypes(List<Behavior> behaviors);
}
