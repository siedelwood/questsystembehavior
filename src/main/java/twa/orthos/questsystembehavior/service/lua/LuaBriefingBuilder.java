
package twa.orthos.questsystembehavior.service.lua;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import twa.orthos.questsystembehavior.model.Briefing;
import twa.orthos.questsystembehavior.model.Page;

/**
 * This class converts the briefings in the inner structure to lua code.
 * 
 * @author totalwarANGEL
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LuaBriefingBuilder implements BriefingBuilder
{

	private List<Briefing> briefings;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String transformBriefings()
	{
		return parseBriefings(briefings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String transformSingleBriefing(final Briefing briefing)
	{
		return parseSingleBriefing(briefing);
	}

	/**
	 * Parses the list of briefings to lua code.
	 * 
	 * @param briefings Briefings
	 * @return Lua string
	 */
	private String parseBriefings(final List<Briefing> briefings)
	{
		String lua = "\n";
		for (int i = 0; i < briefings.size(); i++)
		{
			lua += parseSingleBriefing(briefings.get(i));
			lua += "\n";
		}
		return lua;
	}

	/**
	 * Parses a single briefing to lua code.
	 * 
	 * @param briefing Briefing
	 * @return Lua string
	 */
	private String parseSingleBriefing(final Briefing briefing)
	{
		String lua = "function " + briefing.getName() + "()";
		lua += "\n    local briefing = {}";
		lua += "\n    local AP, ASP = AddPages(briefing)";
		lua += parsePages(briefing.getPages());
		lua += "    briefing.finished = function() end";
		lua += "\n    return StartBriefing(briefing)";
		lua += "\nend";
		return lua;
	}

	/**
	 * Parses a list of pages to lua code.
	 * 
	 * @param pages Page list
	 * @return Lua string
	 */
	private String parsePages(final List<Page> pages)
	{
		String lua = "\n";
		for (int i = 0; i < pages.size(); i++)
		{
			lua += "    " + parseSinglePage(pages.get(i));
		}
		return lua;
	}

	/**
	 * Parses a single page to lua code.
	 * 
	 * @param page Page
	 * @return Lua string
	 */
	private String parseSinglePage(final Page page)
	{
		String action = page.getAction();
		if (action.equals(""))
		{
			action = "nil";
		}
		return String.format(
			"ASP(\"%s\", \"%s\", \"%s\", %b, %s, %b, %b)\n", page.getPosition(), page.getTitle(), page.getText(),
			page.isDialog(), action, page.isSkyShown(), page.isFogDisabled()
		);
	}
}
