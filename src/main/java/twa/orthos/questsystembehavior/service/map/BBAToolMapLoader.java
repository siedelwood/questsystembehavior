
package twa.orthos.questsystembehavior.service.map;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import org.apache.commons.lang.SystemUtils;

/**
 * Implementation for a map loader that uses the external BBA tol by yoq.
 * 
 * @author totalwarANGEL
 *
 */
public class BBAToolMapLoader implements MapLoader
{

	protected String mapPath;

	/**
	 * Constructor
	 */
	public BBAToolMapLoader()
	{
		mapPath = "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void selectMap(final String filename) throws MapLoaderException
	{
		mapPath = filename;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void packMap() throws MapLoaderException
	{
		final File result = new File(mapPath.substring(0, mapPath.length() - 9));
		try
		{
			System.out.println(result.getPath());
			if (result.exists())
			{
				System.out.println("Deleting old version...");
				Files.delete(Paths.get(result.getAbsolutePath()));
				System.out.println("Done!");
			}
		}
		catch (final Exception e)
		{
			throw new MapLoaderException(e);
		}

		final File f = new File(mapPath);
		if (!f.exists() || !f.isDirectory())
		{
			throw new MapLoaderException("Could not pack map: " + mapPath);
		}
		execute();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void unpackMap() throws MapLoaderException
	{
		final File result = new File(mapPath + ".unpacked");
		try
		{
			System.out.println("Deleting unpacked...");
			if (result.exists())
			{
				Files.walk(Paths.get(result.getAbsolutePath())).sorted(Comparator.reverseOrder()).map(Path::toFile)
					.peek(System.out::println).forEach(File::delete);
			}
			System.out.println("Done!");
		}
		catch (final Exception e)
		{
			throw new MapLoaderException(e);
		}

		final File f = new File(mapPath);
		if (!f.exists() || f.isDirectory())
		{
			throw new MapLoaderException("Could not unpack map: " + mapPath);
		}
		execute();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(final String source, String dest) throws MapLoaderException
	{
		dest = (dest == null) ? "" : dest;
		final File destFile = new File(mapPath + "/" + dest);
		destFile.mkdirs();
		try
		{
			final Path sourcePath = Paths.get(source);
			final Path destPath = Paths.get(destFile.getAbsolutePath());
			destPath.toFile().delete();
			System.out.println("copy " + sourcePath + " " + destPath);
			Files.copy(sourcePath, destPath);
		}
		catch (final Exception e)
		{
			throw new MapLoaderException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(final InputStream source, String dest) throws MapLoaderException
	{
		dest = (dest == null) ? "" : dest;
		final File destFile = new File(mapPath + "/" + dest);

		destFile.mkdirs();
		try
		{
			final Path destPath = Paths.get(destFile.getAbsolutePath());
			destPath.toFile().delete();
			System.out.println("copy stream to " + destPath);
			Files.copy(source, destPath);
		}
		catch (final Exception e)
		{
			throw new MapLoaderException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(final String filename) throws MapLoaderException
	{

	}

	/**
	 * Returns if the operating system is windows.
	 * 
	 * @return Is windows
	 */
	protected boolean isWindows()
	{
		return SystemUtils.IS_OS_WINDOWS;
	}

	/**
	 * Builds the command string for the bba tool.
	 * 
	 * @param path Map path
	 * @return Command string
	 */
	protected String buildExecutionString(final String path)
	{
		String exec = System.getProperty("user.dir") + "\\bin\\bba5.exe " + path + "";
		if (!isWindows())
		{
			exec = System.getProperty("user.dir") + "/bin/bba5.sh " + path;
		}
		return exec;
	}

	/**
	 * Executes the bba command.
	 * 
	 * @throws MapLoaderException
	 */
	protected void execute() throws MapLoaderException
	{
		try
		{
			System.out.println("Processing map...");

			System.out.println(mapPath);
			final Process process = Runtime.getRuntime().exec(buildExecutionString(mapPath));
			final InputStream is = process.getInputStream();

			int size = 0;
			final byte[] buffer = new byte[1024];
			while ((size = is.read(buffer)) != -1)
			{
				System.out.write(buffer, 0, size);
			}

			process.waitFor();

			System.out.println("Done!");
		}
		catch (final Exception e)
		{
			throw new MapLoaderException(e);
		}
	}
}
