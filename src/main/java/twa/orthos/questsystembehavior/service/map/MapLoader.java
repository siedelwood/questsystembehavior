
package twa.orthos.questsystembehavior.service.map;

import java.io.InputStream;

/**
 * Interface for map loaders.
 * 
 * @author totalwarANGEL
 *
 */
public interface MapLoader
{
	/**
	 * Selects a map file that will be processed by the map loader.
	 * 
	 * @param filename Map to select
	 * @throws MapLoaderException
	 */
	public void selectMap(String filename) throws MapLoaderException;

	/**
	 * Packs the map.
	 * 
	 * @throws MapLoaderException
	 */
	public void packMap() throws MapLoaderException;

	/**
	 * Unpacks the map.
	 * 
	 * @throws MapLoaderException
	 */
	public void unpackMap() throws MapLoaderException;

	/**
	 * Copies a file to new location inside the map.
	 * 
	 * @param source Source file to copy
	 * @param dest Destinated location
	 * @throws MapLoaderException
	 */
	public void add(String source, String dest) throws MapLoaderException;

	/**
	 * Writes an input stream as file to a location inside the map.
	 * 
	 * @param source Source stream
	 * @param dest Destinated location
	 * @throws MapLoaderException
	 */
	public void add(InputStream source, String dest) throws MapLoaderException;

	/**
	 * Removes a file from the map.
	 * 
	 * @param filename
	 * @throws MapLoaderException
	 */
	public void remove(String filename) throws MapLoaderException;
}
