
package twa.orthos.questsystembehavior.service.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import twa.orthos.questsystembehavior.gui.tree.TreeUserDataBriefing;
import twa.orthos.questsystembehavior.gui.tree.TreeUserDataPage;
import twa.orthos.questsystembehavior.model.Briefing;
import twa.orthos.questsystembehavior.model.BriefingModel;
import twa.orthos.questsystembehavior.model.Page;
import twa.orthos.questsystembehavior.model.PageModel;

/**
 * Mapper for turning BriefingModel into MutableTreeNode and vice versa.
 * 
 * @author totalwarANGEL
 *
 */
public class MutableTreeNodeMapper
{
	/**
	 * Converts a tree node into a page model.
	 * 
	 * @param data Tree node to convert
	 * @return Page model
	 */
	public Page toPage(final MutableTreeNode data)
	{
		final PageModel page = new PageModel();
		page.setPosition(((TreeUserDataPage) ((DefaultMutableTreeNode) data).getUserObject()).getPagePosition());
		page.setTitle(((TreeUserDataPage) ((DefaultMutableTreeNode) data).getUserObject()).getPageTitle());
		page.setText(((TreeUserDataPage) ((DefaultMutableTreeNode) data).getUserObject()).getPageText());
		page.setAction(((TreeUserDataPage) ((DefaultMutableTreeNode) data).getUserObject()).getPageAction());
		page.setDialog(((TreeUserDataPage) ((DefaultMutableTreeNode) data).getUserObject()).isDialogPage());
		return page;
	}

	/**
	 * Converts a page model into a tree node.
	 * 
	 * @param data Page to convert
	 * @param index Index of page
	 * @return Tree node
	 */
	public MutableTreeNode toPageTreeNode(final Page data, final int index)
	{
		final TreeUserDataPage pageData = new TreeUserDataPage();
		pageData.setCaption("" + (index + 1));
		pageData.setPagePosition(data.getPosition());
		pageData.setPageTitle(data.getTitle());
		pageData.setPageText(data.getText());
		pageData.setPageAction(data.getAction());
		pageData.setDialogPage(data.isDialog());
		return new DefaultMutableTreeNode(pageData);
	}

	/**
	 * Converts a MutableTreeNode to a Briefing.
	 * 
	 * @param data Tree node
	 * @return Briefing
	 */
	public Briefing toBriefing(final MutableTreeNode data)
	{
		final BriefingModel briefing = new BriefingModel();
		final DefaultMutableTreeNode userData = (DefaultMutableTreeNode) data;

		final List<Page> pages = new ArrayList<>();
		if (data.getChildCount() > 0)
		{
			for (int i = 0; i < data.getChildCount(); i++)
			{
				pages.add(toPage((MutableTreeNode) userData.getChildAt(i)));
			}
		}

		briefing.setName(((TreeUserDataBriefing) userData.getUserObject()).getCaption());
		briefing.setPages(pages);
		return briefing;
	}

	/**
	 * Converts a Briefing model into a MutableTreeNode.
	 * 
	 * @param briefing Briefing
	 * @return Tree node
	 */
	public MutableTreeNode toBriefingTreeNode(final Briefing briefing)
	{
		final TreeUserDataBriefing rootData = new TreeUserDataBriefing();
		rootData.setCaption(briefing.getName());

		final DefaultMutableTreeNode root = new DefaultMutableTreeNode(briefing.getName());
		root.setUserObject(rootData);

		if (briefing.getPages().size() > 0)
		{
			for (int i = 0; i < briefing.getPages().size(); i++)
			{
				final DefaultMutableTreeNode page = (DefaultMutableTreeNode) toPageTreeNode(
					briefing.getPages().get(i), i
				);
				root.add(page);
				page.setParent(root);
			}
		}
		return root;
	}
}
