package twa.orthos.questsystembehavior.service.save.briefing.json;

import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import twa.orthos.questsystembehavior.model.Briefing;
import twa.orthos.questsystembehavior.model.BriefingModel;
import twa.orthos.questsystembehavior.model.Page;
import twa.orthos.questsystembehavior.model.PageModel;
import twa.orthos.questsystembehavior.service.AbstractJsonLoader;
import twa.orthos.questsystembehavior.service.save.briefing.BriefingReader;

/**
 * 
 * @author totalwarANGEL
 *
 */
public class JsonBriefingReader extends AbstractJsonLoader implements BriefingReader
{

	public JsonBriefingReader()
	{
		super();
	}

	@Override
	public List<Briefing> parseBriefings()
	{
		final JSONArray briefings = (JSONArray) data.get("Briefings");
		
		final List<Briefing> briefingList = new ArrayList<>();
		for (int i = 0; i < briefings.size(); i++)
		{
			briefingList.add(parseSingleBriefing((JSONObject) briefings.get(i)));
		}
		return briefingList;
	}

	@SuppressWarnings("unchecked")
	private Briefing parseSingleBriefing(final JSONObject briefingData)
	{
		final BriefingModel briefing = new BriefingModel();
		briefing.setName((String) briefingData.get("Name"));
		
		final List<JSONArray> pages = (List<JSONArray>) briefingData.get("Pages");
		if (pages.size() > 0) 
		{
			for (int i=0; i<pages.size(); i++)
			{
				briefing.addPage(parsePage(pages.get(i)));
			}
		}
		return briefing;
	}

	private Page parsePage(final JSONArray pageData) 
	{
		final PageModel page = new PageModel();
		page.setPosition((String) pageData.get(0));
		page.setTitle((String) pageData.get(1));
		page.setText((String) pageData.get(2));
		page.setDialog((boolean) pageData.get(3));
		page.setAction((String) pageData.get(4));
		page.setSkyShown((boolean) pageData.get(5));
		page.setFogDisabled((boolean) pageData.get(6));
		return page;
	}
}
