
package twa.orthos.questsystembehavior.service.save.settings;

import twa.orthos.questsystembehavior.model.MapSettings;

public interface SettingsSerializer
{
	String serializeSettings(MapSettings mapSettings);

}
