
package twa.orthos.questsystembehavior.service.save.behavior;

import java.util.List;
import twa.orthos.questsystembehavior.model.Behavior;
import twa.orthos.questsystembehavior.model.Parameter;

/**
 * Interface for behavor reader.
 * @author mheller
 *
 */
public interface BehaviorReader
{
	/**
	 * Returns all behaviors.
	 * 
	 * @return Behavior list
	 */
	public List<Behavior> parseBehavior();

	/**
	 * Returns all parameters of the behavior type.
	 * 
	 * @param name Behavior name
	 * @return List of parameters
	 */
	public List<Parameter> parseParameter(final String name);

	/**
	 * Loads a file containing the behavior data.
	 * 
	 * @param fileName Absolute path
	 * @throws Exception
	 */
	public void load(final String fileName) throws Exception;
}
