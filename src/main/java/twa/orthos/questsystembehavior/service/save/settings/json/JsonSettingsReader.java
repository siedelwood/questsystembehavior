
package twa.orthos.questsystembehavior.service.save.settings.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import twa.orthos.questsystembehavior.model.MapSettings;
import twa.orthos.questsystembehavior.model.MapSettingsModel;
import twa.orthos.questsystembehavior.service.AbstractJsonLoader;
import twa.orthos.questsystembehavior.service.save.settings.SettingsReader;

public class JsonSettingsReader extends AbstractJsonLoader implements SettingsReader
{
	public JsonSettingsReader()
	{
		super();
	}

	@Override
	public MapSettings parseSettings()
	{
		final MapSettings settings = new MapSettingsModel();
		settings.fromJson((JSONObject) ((JSONArray) data.get("Settings")).get(0));
		return settings;
	}

}
