
package twa.orthos.questsystembehavior.service.save.quest.json;

import java.util.List;
import twa.orthos.questsystembehavior.model.Quest;
import twa.orthos.questsystembehavior.service.save.quest.QuestSerializer;

public class JsonQuestSerializer implements QuestSerializer
{
	public JsonQuestSerializer()
	{
	}

	@Override
	public String serializeQuests(final List<Quest> quests)
	{
		return String.format("\"Quests\": [%s]", convertQuests(quests));
	}
	
	@Override
	public String serializeQuest(final Quest quest)
	{
		return convertQuest(quest);
	}

	protected String convertQuests(final List<Quest> quests)
	{
		String serialized = "";
		for (int i=0; i<quests.size(); i++) {
			if (i > 0) {
				serialized += ",";
			}
			serialized += convertQuest(quests.get(i));
		}
		return serialized;
	}

	protected String convertQuest(final Quest quest)
	{
		String questString = "{";
		questString += "\"Name\": \""       + quest.getName() + "\",";
		questString += "\"Time\": \""       + quest.getTime() + "\",";
		questString += "\"Receiver\": \""   + quest.getReceiver() + "\",";
		questString += "\"QuestText\": \""  + quest.getQuestText() + "\",";
		questString += "\"QuestTitle\": \"" + quest.getQuestTitle() + "\",";
		questString += "\"QuestType\": \""  + quest.getQuestType() + "\",";
		questString += "\"Visible\": " + quest.isVisible() + ",";
		
		final List<List<String>> behavior = quest.getBehavior();
		questString += "\"Behavior\": [";
		for (int i=0; i<behavior.size(); i++) {
			if (i > 0) {
				questString += ",";
			}
			questString += "[";
			for (int j=0; j<behavior.get(i).size(); j++) {
				if (j > 0) {
					questString += ",";
				}
				questString += "\"" + behavior.get(i).get(j) + "\"";
			}
			questString += "]";
		}
		questString += "]";
		
		questString += "}";
		return questString;
	}

	
}
