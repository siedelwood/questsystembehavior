
package twa.orthos.questsystembehavior.service.save.quest.json;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import twa.orthos.questsystembehavior.model.Quest;
import twa.orthos.questsystembehavior.model.QuestModel;
import twa.orthos.questsystembehavior.service.AbstractJsonLoader;
import twa.orthos.questsystembehavior.service.save.quest.QuestReader;

public class JsonQuestReader extends AbstractJsonLoader implements QuestReader
{
	public JsonQuestReader()
	{
		super();
	}

	@Override
	public List<Quest> parseQuests()
	{
		final JSONArray quests = (JSONArray) data.get("Quests");

		final List<Quest> questList = new ArrayList<>();
		for (int i = 0; i < quests.size(); i++)
		{
			questList.add(parseSingleQuest((JSONObject) quests.get(i)));
		}
		
		final Comparator<Quest> c = (a, b) -> a.getName().compareTo(b.getName());
		questList.sort(c);
		return questList;
	}

	@SuppressWarnings("unchecked")
	protected Quest parseSingleQuest(final JSONObject rawQuest)
	{
		final QuestModel model = new QuestModel();
		model.setName((String) rawQuest.get("Name"));
		model.setTime((String) rawQuest.get("Time"));
		model.setReceiver((String) rawQuest.get("Receiver"));
		model.setQuestText((String) rawQuest.get("QuestText"));
		model.setQuestTitle((String) rawQuest.get("QuestTitle"));
		model.setQuestType((String) rawQuest.get("QuestType"));
		model.setVisible((boolean) rawQuest.get("Visible"));
		model.setBehavior((List<List<String>>) rawQuest.get("Behavior"));
		return model;
	}
}
