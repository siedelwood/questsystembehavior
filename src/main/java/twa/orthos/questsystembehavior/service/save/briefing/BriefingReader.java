package twa.orthos.questsystembehavior.service.save.briefing;

import java.util.List;
import twa.orthos.questsystembehavior.model.Briefing;

public interface BriefingReader
{
	public List<Briefing> parseBriefings();
	
	public void load(final String fileName) throws Exception;
}
