
package twa.orthos.questsystembehavior.service.save.settings;

import twa.orthos.questsystembehavior.model.MapSettings;

public interface SettingsReader
{
	public void load(final String fileName) throws Exception;

	public MapSettings parseSettings();
}
