package twa.orthos.questsystembehavior.service.save.briefing;

import java.util.List;
import twa.orthos.questsystembehavior.model.Briefing;

public interface BriefingSerializer
{
	String serializeBriefings(List<Briefing> briefings);

	String serializeBriefing(Briefing briefing);
}
