
package twa.orthos.questsystembehavior.service.save.settings.json;

import twa.orthos.questsystembehavior.model.MapSettings;
import twa.orthos.questsystembehavior.service.save.settings.SettingsSerializer;

public class JsonSettingsSerializer implements SettingsSerializer
{
	@Override
	public String serializeSettings(final MapSettings mapSettings)
	{
		return "\"Settings\": [" + mapSettings.toJson() + "]";
	}

}
