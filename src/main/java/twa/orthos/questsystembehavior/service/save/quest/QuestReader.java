package twa.orthos.questsystembehavior.service.save.quest;

import java.util.List;
import twa.orthos.questsystembehavior.model.Quest;

public interface QuestReader
{
	public List<Quest> parseQuests();
	
	public void load(final String fileName) throws Exception;
}
