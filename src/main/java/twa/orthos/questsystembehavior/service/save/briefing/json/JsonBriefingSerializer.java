
package twa.orthos.questsystembehavior.service.save.briefing.json;

import java.util.List;
import twa.orthos.questsystembehavior.model.Briefing;
import twa.orthos.questsystembehavior.model.Page;
import twa.orthos.questsystembehavior.service.save.briefing.BriefingSerializer;

/**
 * 
 * @author totalwarANGEL
 *
 */
public class JsonBriefingSerializer implements BriefingSerializer
{
	/**
	 * Constructor
	 */
	public JsonBriefingSerializer()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String serializeBriefings(final List<Briefing> briefings)
	{
		return String.format("\"Briefings\": [%s]", convertBriefings(briefings));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String serializeBriefing(final Briefing briefing)
	{
		return convertBriefing(briefing);
	}

	/**
	 * Serializes the generated briefings to json.
	 * 
	 * @param briefings Briefings to serialize
	 * @return Serialized briefings
	 */
	private Object convertBriefings(final List<Briefing> briefings)
	{
		final StringBuilder builder = new StringBuilder();
		for (int i = 0; i < briefings.size(); i++)
		{
			if (i > 0)
			{
				builder.append(",");
			}
			builder.append(convertBriefing(briefings.get(i)));
		}
		return builder.toString();
	}

	/**
	 * Serializes a single briefing.
	 * 
	 * @param briefing Briefing to serialize
	 * @return Serialized briefing
	 */
	private String convertBriefing(final Briefing briefing)
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("{").append("\"Name\":\"").append(briefing.getName()).append("\",");

		String serialized = "{";

		serialized += "\"Name\":\"" + briefing.getName() + "\",";
		serialized += "\"Pages\":" + convertPages(briefing.getPages());

		serialized += "}";
		return serialized;
	}

	/**
	 * Serializes the pages of the briefing.
	 * 
	 * @param pages Page list
	 * @return Serialized page list
	 */
	private String convertPages(final List<Page> pages)
	{
		String serialized = "[";

		for (int i = 0; i < pages.size(); i++)
		{
			if (i > 0)
			{
				serialized += ",";
			}
			serialized += "[";
			serialized += "\"" + pages.get(i).getPosition() + "\",";
			serialized += "\"" + pages.get(i).getTitle() + "\",";
			serialized += "\"" + pages.get(i).getText() + "\",";
			serialized += "" + pages.get(i).isDialog() + ",";
			serialized += "\"" + pages.get(i).getAction() + "\",";
			serialized += "" + pages.get(i).isSkyShown() + ",";
			serialized += "" + pages.get(i).isFogDisabled() + "";
			serialized += "]";
		}

		serialized += "]";
		return serialized;
	}
}
