
package twa.orthos.questsystembehavior.service.save.quest;

import java.util.List;
import twa.orthos.questsystembehavior.model.Quest;

public interface QuestSerializer
{
	String serializeQuests(List<Quest> quests);

	String serializeQuest(Quest quest);
}
