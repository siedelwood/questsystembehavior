
package twa.orthos.questsystembehavior.service.save.behavior.json;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import twa.orthos.questsystembehavior.model.Behavior;
import twa.orthos.questsystembehavior.model.BehaviorModel;
import twa.orthos.questsystembehavior.model.Parameter;
import twa.orthos.questsystembehavior.model.ParameterModel;
import twa.orthos.questsystembehavior.service.AbstractJsonLoader;
import twa.orthos.questsystembehavior.service.save.behavior.BehaviorReader;

/**
 * Implementation for a behavior reader that transforms behavior data from JSON
 * to behavior models.
 * 
 * @author totalwarANGEL
 *
 */
public class JsonBehaviorReader extends AbstractJsonLoader implements BehaviorReader
{
	/**
	 * Constructor
	 * 
	 * @param confPath Configuration file
	 */
	public JsonBehaviorReader()
	{
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Behavior> parseBehavior()
	{
		final List<Behavior> behaviors = new ArrayList<>();

		final Iterator<?> it = data.entrySet().iterator();
		while (it.hasNext())
		{
			final Map.Entry pair = (Map.Entry) it.next();
			behaviors.add(parseSingleBehavior((String) pair.getKey()));
		}

		final Comparator c = (a, b) -> ((Behavior) a).getName().compareTo(((Behavior) b).getName());
		behaviors.sort(c);
		return behaviors;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Parameter> parseParameter(final String name)
	{
		final List<Parameter> parameter = new ArrayList<>();

		final JSONArray behaviorParameter = (JSONArray) ((JSONObject) data.get(name)).get("Parameter");
		for (int i = 0; i < behaviorParameter.size(); i++)
		{
			final ParameterModel p = new ParameterModel(
				(String) ((JSONArray) behaviorParameter.get(i)).get(0), (String) ((JSONArray) behaviorParameter.get(i)).get(1), (List<String>) ((JSONArray) behaviorParameter.get(i)).get(2)
			);
			parameter.add(p);
		}
		return parameter;
	}

	/**
	 * Parses a single behavior.
	 * 
	 * @param key Behavior name
	 * @return parsed behavior
	 */
	@SuppressWarnings("unchecked")
	private Behavior parseSingleBehavior(final String key)
	{
		final BehaviorModel behavior = new BehaviorModel();
		behavior.setName(key);
		behavior.setDescription((String) ((JSONObject) data.get(key)).get("Description"));

		final List<List<Object>> parameter = new ArrayList<>();
		final JSONArray fields = (JSONArray) ((JSONObject) data.get(key)).get("Parameter");
		for (int i = 0; i < fields.size(); i++)
		{
			parameter.add((JSONArray) fields.get(i));
		}
		behavior.setFields(parameter);
		return behavior;
	}
}
