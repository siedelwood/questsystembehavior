
package twa.orthos.questsystembehavior.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Shared behavior of all classes that are loading mission data from JSON.
 * 
 * @author totalwarANGEL
 *
 */
public abstract class AbstractJsonLoader
{
	protected JSONParser parser;

	protected JSONObject data;

	/**
	 * Constructor
	 */
	protected AbstractJsonLoader()
	{
		parser = new JSONParser();
	}

	/**
	 * Loads the JSON content from the file at the given path.
	 * 
	 * @param fileName Absolute path
	 * @throws Exception
	 */
	public void load(final String fileName) throws Exception
	{
	    BufferedReader in;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
			data = (JSONObject) ((JSONArray) parser.parse(in)).get(0);
		}
		catch (final Exception e)
		{
			throw new Exception(e);
		}
	}
}
