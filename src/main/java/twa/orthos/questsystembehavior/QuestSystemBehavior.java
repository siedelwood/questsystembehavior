
package twa.orthos.questsystembehavior;

import javax.swing.JFrame;

import twa.orthos.questsystembehavior.container.briefing.BriefingsContainer;
import twa.orthos.questsystembehavior.container.quest.BehaviorTypeContainer;
import twa.orthos.questsystembehavior.container.quest.QuestsContainer;
import twa.orthos.questsystembehavior.controller.BriefingLogicController;
import twa.orthos.questsystembehavior.controller.MainWindowController;
import twa.orthos.questsystembehavior.controller.MapSettingsLogicController;
import twa.orthos.questsystembehavior.controller.QuestLogicController;
import twa.orthos.questsystembehavior.model.MapSettings;
import twa.orthos.questsystembehavior.model.MapSettingsModel;
import twa.orthos.questsystembehavior.service.FileChooserService;
import twa.orthos.questsystembehavior.service.map.BBAToolMapLoader;
import twa.orthos.questsystembehavior.service.save.behavior.BehaviorReader;
import twa.orthos.questsystembehavior.service.save.behavior.json.JsonBehaviorReader;
import twa.orthos.questsystembehavior.service.save.briefing.BriefingReader;
import twa.orthos.questsystembehavior.service.save.briefing.BriefingSerializer;
import twa.orthos.questsystembehavior.service.save.briefing.json.JsonBriefingReader;
import twa.orthos.questsystembehavior.service.save.briefing.json.JsonBriefingSerializer;
import twa.orthos.questsystembehavior.service.save.quest.QuestReader;
import twa.orthos.questsystembehavior.service.save.quest.QuestSerializer;
import twa.orthos.questsystembehavior.service.save.quest.json.JsonQuestReader;
import twa.orthos.questsystembehavior.service.save.quest.json.JsonQuestSerializer;
import twa.orthos.questsystembehavior.service.save.settings.SettingsReader;
import twa.orthos.questsystembehavior.service.save.settings.SettingsSerializer;
import twa.orthos.questsystembehavior.service.save.settings.json.JsonSettingsReader;
import twa.orthos.questsystembehavior.service.save.settings.json.JsonSettingsSerializer;
import twa.orthos.questsystembehaviormanual.controller.ManualLogicController;
import twa.orthos.questsystembehaviormanual.service.content.JsonManualContentReader;
import twa.orthos.questsystembehaviormanual.service.content.ManualContentReader;

/**
 * Main class of the assistant.
 * 
 * @author totalwarANGEL
 */
public class QuestSystemBehavior
{

    /**
     * Creates the quest controller.
     * 
     * @return           Quest controller
     * @throws Exception
     */
    public static QuestLogicController prepareQuestController() throws Exception
    {
        final QuestsContainer questContainer = QuestsContainer.getInstance();
        final BehaviorTypeContainer behaviorContainer = BehaviorTypeContainer.getInstance();
        final QuestSerializer questSerializer = new JsonQuestSerializer();

        final BehaviorReader behaviorReader = new JsonBehaviorReader();
        behaviorReader.load("cnf/behavior.json");
        final QuestReader questReader = new JsonQuestReader();

        final QuestLogicController questController = new QuestLogicController(
            questReader, behaviorReader, questSerializer, questContainer, behaviorContainer
        );
        return questController;
    }

    /**
     * Creates the briefing controller.
     * 
     * @return           Briefing controller
     * @throws Exception
     */
    public static BriefingLogicController prepareBriefingController() throws Exception
    {
        final BriefingsContainer briefingConrainer = BriefingsContainer.getInstance();
        final BriefingReader briefingReader = new JsonBriefingReader();
        final BriefingSerializer briefingSerializer = new JsonBriefingSerializer();

        final BriefingLogicController briefingController = new BriefingLogicController(
            briefingConrainer, briefingReader, briefingSerializer
        );
        return briefingController;
    }

    /**
     * Creates the map controller.
     * 
     * @return           Map controller
     * @throws Exception
     */
    public static MapSettingsLogicController prepareMapController() throws Exception
    {
        final SettingsReader settingsReader = new JsonSettingsReader();
        final SettingsSerializer settingsSerializer = new JsonSettingsSerializer();
        final MapSettings mapSettings = new MapSettingsModel();

        final MapSettingsLogicController mapController = new MapSettingsLogicController(
            mapSettings, settingsReader, settingsSerializer
        );
        return mapController;
    }

    /**
     * Creates the logic controller for the manual tab.
     * 
     * @return Manual logic controller
     */
    public static ManualLogicController prepareManualController()
    {
        final ManualContentReader reader = new JsonManualContentReader();
        final JFrame frame = new JFrame();

        final ManualLogicController logic = new ManualLogicController();
        logic.setReader(reader);
        logic.loadManualData("cnf/manual.json");
        return logic;
    }

    /**
     * Main method
     * 
     * @param  args      Program args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception
    {
        try
        {
            final FileChooserService fcs = new FileChooserService();

            final MainWindowController ic = new MainWindowController();
            ic.setQuestController(prepareQuestController());
            ic.setBriefingController(prepareBriefingController());
            ic.setMapController(prepareMapController());
            ic.setManualController(prepareManualController());
            ic.setMapLoader(new BBAToolMapLoader());
            ic.setFileChooser(fcs);
            ic.run(810, 700);
        } catch (final Exception e)
        {
            throw e;
        }
    }
}
