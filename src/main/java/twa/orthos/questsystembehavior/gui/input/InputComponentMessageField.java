
package twa.orthos.questsystembehavior.gui.input;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import twa.orthos.questsystembehavior.gui.swing.JRegexTextArea;

/**
 * Input component implementation for string input requests.
 * 
 * @author totalwarANGEL
 */
@SuppressWarnings("serial")
public class InputComponentMessageField extends JPanel implements InputComponent, DocumentListener
{
	protected JLabel description;

	protected JRegexTextArea value;

	protected int w;

	protected int h;

	/**
	 * Constructor
	 * 
	 * @param w Width
	 * @param h Height
	 */
	public InputComponentMessageField(final int w, final int h)
	{
		this.w = w;
		this.h = h;
		build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCaption(final String text)
	{
		description.setText(text);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCaption()
	{
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getValue()
	{
		String text = value.getText();
		text = text.replaceAll("\n", " @cr ");
		return text;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getPane()
	{
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(final String string)
	{
		value.setText(string);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset()
	{
		value.setText("");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertUpdate(final DocumentEvent e)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeUpdate(final DocumentEvent e)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void changedUpdate(final DocumentEvent e)
	{

	}

	/**
	 * Initializes the parts of the input component.
	 */
	private void build()
	{
		setLayout(null);
		setBounds(0, 0, w, h);

		description = new JLabel("");
		description.setBounds(2, 2, (int) ((w * 0.4) - 8), 20);
		add(description);

		final JScrollPane scrollPaneDesc = new JScrollPane();
		value = new JRegexTextArea("[^ \n0-9a-zäöüß@A-ZÄÖÜ.:\\-_!?'§\\$%/\\(\\)]");
		scrollPaneDesc.setViewportView(value);
		scrollPaneDesc.setBounds((int) ((w * 0.4) + 2), 2, (int) ((w * 0.6) - 8), h - 4);
		scrollPaneDesc.setVisible(true);
		value.build();
		add(scrollPaneDesc);

		setVisible(true);
	}

	/**
	 * Main method for testing.
	 * 
	 * @param args Program arguments
	 */
	public static void main(final String[] args)
	{
		final int width = 502;
		final int height = 40;

		final JFrame f = new JFrame();
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		f.setLocationRelativeTo(null);

		final InputComponentMessageField test = new InputComponentMessageField(width, 40);
		test.setCaption("Some Value");
		
		f.setSize(width, height + test.getHeight());
		f.add(test);

		f.setVisible(true);
	}
}
