
package twa.orthos.questsystembehavior.gui.input;

import java.util.List;
import twa.orthos.questsystembehavior.model.Parameter;

/**
 * Factory for input components.
 * 
 * @author totalwarANGEL
 *
 */
public interface InputComponentFactory
{
	/**
	 * Creates a boolean selection component for a dialog window.
	 * 
	 * @param w Width of component
	 * @param h Height of component
	 * @param caption Label text
	 * @return Boolean selection component
	 */
	public InputComponentBooleanBox getBooleanBox(final int w, final int h, final String caption);

	/**
	 * Creates a integer declaration component for a dialog window.
	 * 
	 * @param w Width of component
	 * @param h Height of component
	 * @param caption Label text
	 * @return Integer component
	 */
	public InputComponentIntegerField getIntegerField(final int w, final int h, final String caption);

	/**
	 * Creates a string declaration component for a dialog window.
	 * 
	 * @param w Width of component
	 * @param h Height of component
	 * @param caption Label text
	 * @return String component
	 */
	public InputComponentStringField getStringField(final int w, final int h, final String caption);

	/**
	 * Creates a integer selection component for a dialog window.
	 * 
	 * @param w Width of component
	 * @param h Height of component
	 * @param caption Label text
	 * @param options Option list
	 * @return Integer selection component
	 */
	public InputComponentIntegerBox getIntegerBox(
		final int w, final int h, final String caption, final List<String> options
	);

	/**
	 * Creates a string selection component for a dialog window.
	 * 
	 * @param w Width of component
	 * @param h Height of component
	 * @param caption Label text
	 * @param options Option list
	 * @return String selection component
	 */
	public InputComponentStringBox getStringBox(
		final int w, final int h, final String caption, final List<String> options
	);

	/**
	 * Creates a string declaration component for a dialog window.
	 * 
	 * @param w Width of component
	 * @param h Height of component
	 * @param caption Label text
	 * @return String component
	 */
	public InputComponentMessageField getMessageField(final int w, final int h, final String caption);

	/**
	 * Creates an input component from parameter data.
	 * 
	 * @param w Width of component
	 * @param h Height of component
	 * @param parameter Parameter data
	 * @return Input component
	 */
	public InputComponent getFromParameterData(final int w, final int h, Parameter parameter);
}
