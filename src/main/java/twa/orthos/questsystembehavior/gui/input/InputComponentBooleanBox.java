
package twa.orthos.questsystembehavior.gui.input;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * Input component implementation for boolean requests.
 * 
 * @author totalwarANGEL
 */
@SuppressWarnings("serial")
public class InputComponentBooleanBox extends JPanel implements InputComponent
{
	protected JLabel description;

	protected JComboBox<Boolean> value;

	protected int w;

	protected int h;

	/**
	 * Constructor
	 * 
	 * @param w Width
	 * @param h Height
	 */
	public InputComponentBooleanBox(final int w, final int h)
	{
		this.w = w;
		this.h = h;
		build();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCaption(final String text)
	{
		description.setText(text);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCaption()
	{
		return description.getText();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getValue()
	{
		return Boolean.toString(value.getItemAt(value.getSelectedIndex()));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getPane()
	{
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(final String string)
	{
		value.setSelectedIndex((string.equals("true") ? 0 : 1));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset()
	{
		value.setSelectedIndex(0);
	}

	/**
	 * Initializes the parts of the input component.
	 */
	private void build()
	{
		setLayout(null);
		setBounds(0, 0, w, h);

		description = new JLabel("");
		description.setBounds(2, 2, (int) ((w * 0.4) - 8), h - 4);
		add(description);

		value = new JComboBox<Boolean>();
		value.addItem(true);
		value.addItem(false);
		value.setBounds((int) (w * 0.4) + 2, 2, (int) (w * 0.6) - 8, h - 4);
		add(value);

		setVisible(true);
	}

	/**
	 * Main method for testing.
	 * 
	 * @param args Program arguments
	 */
	public static void main(final String[] args)
	{
		final int width = 502;
		final int height = 100;

		final JFrame f = new JFrame();
		f.setSize(width, height);
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		f.setLocationRelativeTo(null);

		final InputComponentBooleanBox test = new InputComponentBooleanBox(width, 22);
		test.setCaption("Some Value");
		f.add(test);

		f.setVisible(true);
	}
}
