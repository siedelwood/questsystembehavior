
package twa.orthos.questsystembehavior.gui.input;

import java.util.List;
import lombok.NoArgsConstructor;
import twa.orthos.questsystembehavior.model.Parameter;

/**
 * Implementation for the input component factory.
 * 
 * @author totalwarANGEL
 *
 */
@NoArgsConstructor
public class InputComponentFactoryImpl implements InputComponentFactory
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputComponentBooleanBox getBooleanBox(final int w, final int h, final String caption)
	{
		final InputComponentBooleanBox component = new InputComponentBooleanBox(w, h);
		component.setCaption(caption);
		return component;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputComponentIntegerField getIntegerField(final int w, final int h, final String caption)
	{
		final InputComponentIntegerField component = new InputComponentIntegerField(w, h);
		component.setCaption(caption);
		return component;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputComponentStringField getStringField(final int w, final int h, final String caption)
	{
		final InputComponentStringField component = new InputComponentStringField(w, h);
		component.setCaption(caption);
		return component;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputComponentMessageField getMessageField(final int w, final int h, final String caption)
	{
		final InputComponentMessageField component = new InputComponentMessageField(w, h);
		component.setCaption(caption);
		return component;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputComponentIntegerBox getIntegerBox(
		final int w, final int h, final String caption, final List<String> options
	)
	{
		final InputComponentIntegerBox component = new InputComponentIntegerBox(w, h);
		component.setCaption(caption);
		component.setOptions(options);
		return component;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputComponentStringBox getStringBox(
		final int w, final int h, final String caption, final List<String> options
	)
	{
		final InputComponentStringBox component = new InputComponentStringBox(w, h);
		component.setCaption(caption);
		component.setOptions(options);
		return component;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputComponent getFromParameterData(final int w, final int h, final Parameter parameter)
	{
		if (parameter.getType().equals("NumberInput"))
		{
			return getIntegerField(w, h, parameter.getName());
		}
		if (parameter.getType().equals("NumberSelect"))
		{
			final InputComponentIntegerBox box = getIntegerBox(w, h, parameter.getName(), parameter.getValues());
			return box;
		}
		if (parameter.getType().equals("StringInput"))
		{
			return getStringField(w, h, parameter.getName());
		}
		if (parameter.getType().equals("StringSelect"))
		{
			return getStringBox(w, h, parameter.getName(), parameter.getValues());
		}
		if (parameter.getType().equals("MessageInput"))
		{
			return getMessageField(w, h, parameter.getName());
		}
		if (parameter.getType().equals("BooleanSelect"))
		{
			return getBooleanBox(w, h, parameter.getName());
		}
		return null;
	}
}
