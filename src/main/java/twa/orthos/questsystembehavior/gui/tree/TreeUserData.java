
package twa.orthos.questsystembehavior.gui.tree;

/**
 * Interface for JTree user data.
 * 
 * @author mheller
 *
 */
public interface TreeUserData
{
	/**
	 * Returns the caption of the node as its string representation.
	 * 
	 * @return
	 */
	@Override
	public String toString();

	/**
	 * Returns a serialization of this tree node.
	 * 
	 * @return Serialized node
	 */
	public String serialize();

	/**
	 * Returns the type of the tree node. The type is always 0, 1 or 2.
	 * 
	 * @return Node type
	 */
	public int getNodeType();

	/**
	 * Sets the caption of the tree node.
	 * 
	 * @param caption Caption of node
	 */
	public void setCaption(String caption);

	/**
	 * Returns the caption of the tree node.
	 * 
	 * @return Caption
	 */
	public String getCaption();

	//
	// Page stuff
	//

	/**
	 * Sets the title of the page.
	 * 
	 * @param title Displayed title
	 */
	public void setPageTitle(String title);

	/**
	 * Returns the title of the page.
	 * 
	 * @return Displayed title
	 */
	public String getPageTitle();

	/**
	 * Sets the text of the page.
	 * 
	 * @param text Displayed text
	 */
	public void setPageText(String text);

	/**
	 * Returns the text of the page.
	 * 
	 * @return Displayed text
	 */
	public String getPageText();

	/**
	 * Sets the position where the camera is looking at.
	 * 
	 * @param position Camera position
	 */
	public void setPagePosition(String position);

	/**
	 * Returns the position where the camera is looking at.
	 * 
	 * @return Camera position
	 */
	public String getPagePosition();

	/**
	 * Sets the function called when page is shown.
	 * 
	 * @param function Function to call
	 */
	public void setPageFunction(String function);

	/**
	 * Returns the function called when page is shown.
	 * 
	 * @return Function to call
	 */
	public String getPageFunction();

	/**
	 * Sets if dialog camera will be used.
	 * 
	 * @param dialog Use dialog
	 */
	public void setDialogPage(boolean dialog);

	/**
	 * Returns if dialog camera will be used.
	 * 
	 * @return Use dialog
	 */
	public boolean isDialogPage();

	/**
	 * Sets if the sky is enabled during a briefing.
	 * 
	 * @param flag Sky enabled
	 */
	void setSkyShown(boolean flag);

	/**
	 * Returns if the sky is enabled during a briefing.
	 * 
	 * @return Sky enabled
	 */
	boolean isSkyShown();

	/**
	 * Sets if the fog of war is disabled during a briefing.
	 * 
	 * @param flag Fog disabled
	 */
	void setFogDisabled(boolean flag);

	/**
	 * Returns if the fog of war is disabled during a briefing.
	 * 
	 * @return Fog disabled
	 */
	boolean isFogDisabled();
}
