
package twa.orthos.questsystembehavior.gui.tree;

/**
 * Abstract base implementation for JTree user data.
 * 
 * @author totalwarANGEL
 *
 */
public abstract class AbstractTreeUserData implements TreeUserData
{
	protected String caption;

	/**
	 * Constructor
	 */
	public AbstractTreeUserData()
	{
	}

	/**
	 * Constructor
	 * 
	 * @param caption Caption
	 */
	public AbstractTreeUserData(final String caption)
	{
		setCaption(caption);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		return caption;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public abstract String serialize();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public abstract int getNodeType();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCaption()
	{
		return caption;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCaption(final String caption)
	{
		this.caption = caption;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPageTitle(final String title)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPageTitle()
	{
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPageText(final String text)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPageText()
	{
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPagePosition(final String position)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPagePosition()
	{
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPageFunction(final String function)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPageFunction()
	{
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDialogPage(final boolean dialog)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDialogPage()
	{
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSkyShown(final boolean dialog)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSkyShown()
	{
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFogDisabled(final boolean dialog)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isFogDisabled()
	{
		return false;
	}
}
