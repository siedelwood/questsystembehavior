
package twa.orthos.questsystembehavior.gui.tree;

import lombok.Getter;
import lombok.Setter;

/**
 * JTree user data implementation for pages.
 * 
 * @author totalwarANGEL
 *
 */
@Getter
@Setter
public class TreeUserDataPage extends AbstractTreeUserData
{
	protected TreeUserDataBriefing parent;
	
	protected String pageTitle;

	protected String pageText;

	protected String pagePosition;

	protected String pageAction;

	protected boolean dialogPage;

	protected boolean showSky;

	protected boolean hideFog;

	/**
	 * Constructor
	 */
	public TreeUserDataPage()
	{
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param caption Caption
	 */
	public TreeUserDataPage(final String caption)
	{
		super(caption);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String serialize()
	{
		final String serial = "[type=2;caption=%s;title=%s;text=%s;position=%s;dialog=%b;action=%s;showSky=%b;hideFog=%b;]";
		return String.format(serial, caption, pageTitle, pageText, pagePosition, dialogPage, pageAction, showSky, hideFog);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getNodeType()
	{
		return 2;
	}
}
