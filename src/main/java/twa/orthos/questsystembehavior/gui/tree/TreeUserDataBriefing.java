
package twa.orthos.questsystembehavior.gui.tree;

import lombok.Getter;
import lombok.Setter;

/**
 * JTree user data implementation for briefings.
 * 
 * @author totalwarANGEL
 *
 */
@Getter
@Setter
public class TreeUserDataBriefing extends AbstractTreeUserData
{
	/**
	 * Constructor
	 */
	public TreeUserDataBriefing()
	{
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param caption Caption
	 */
	public TreeUserDataBriefing(final String caption)
	{
		super(caption);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String serialize()
	{
		final String serial = "[type=1;caption=" + caption + ";]";
		return serial;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getNodeType()
	{
		return 1;
	}
}
