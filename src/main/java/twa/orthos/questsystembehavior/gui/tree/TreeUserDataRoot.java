
package twa.orthos.questsystembehavior.gui.tree;

/**
 * JTree user data implementation for the tree root.
 * 
 * @author totalwarANGEL
 *
 */
public class TreeUserDataRoot extends AbstractTreeUserData
{
	/**
	 * Constructor
	 */
	public TreeUserDataRoot()
	{
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param caption Caption
	 */
	public TreeUserDataRoot(final String caption)
	{
		super(caption);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String serialize()
	{
		final String serial = "[type=0;caption=" + caption + ";]";
		return serial;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getNodeType()
	{
		return 0;
	}
}
