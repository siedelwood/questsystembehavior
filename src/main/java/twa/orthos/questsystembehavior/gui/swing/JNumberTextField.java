
package twa.orthos.questsystembehavior.gui.swing;

/**
 * A text field extension that only accepts numbers.
 * 
 * @author totalwarANGEL
 */
@SuppressWarnings("serial")
public class JNumberTextField extends JRegexTextField
{
	public JNumberTextField()
	{
		super("", "\\D", "0");
	}

	public JNumberTextField(final String text)
	{
		super(text, "\\D", "0");
	}
}
