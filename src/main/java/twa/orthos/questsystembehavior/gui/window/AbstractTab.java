
package twa.orthos.questsystembehavior.gui.window;

import javax.swing.JPanel;

/**
 * Shared base class of all assistant tabs.
 * 
 * @author totalwarANGEL
 */
@SuppressWarnings("serial")
public abstract class AbstractTab extends JPanel
{
	protected String caption;

	protected int w;

	protected int h;

	/**
	 * Constructor
	 * 
	 * @param x WIdth
	 * @param y Height
	 * @param caption Caption
	 */
	public AbstractTab(final int w, final int h, final String caption)
	{
		super(null);
		setSize(w, h);
		this.caption = caption;
		this.w = w;
		this.h = h;
	}

	/**
	 * Enables or disables the tab components.
	 * 
	 * @param flag Enabled flag
	 */
	@Override
	public void setEnabled(final boolean flag)
	{
		// To be overridden!
	}

	/**
	 * Sets the caption of the tab.
	 * 
	 * @param caption
	 */
	public void setTabCaption(final String caption)
	{
		this.caption = caption;
	}

	/**
	 * Returns the caption of the tab.
	 * 
	 * @return Caption of tab
	 */
	public String getTabCaption()
	{
		return caption;
	}
}
