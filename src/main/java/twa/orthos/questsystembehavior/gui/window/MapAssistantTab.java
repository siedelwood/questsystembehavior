
package twa.orthos.questsystembehavior.gui.window;

import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import twa.orthos.questsystembehavior.controller.MapSettingsGuiController;
import twa.orthos.questsystembehavior.gui.swing.JNumberTextField;
import twa.orthos.questsystembehavior.gui.swing.JProsaTextField;

/**
 * This class implements the graphical user interface of the map settings tab.
 * 
 * @author totalwarANGEL
 */
@SuppressWarnings("serial")
public class MapAssistantTab extends AbstractTab
{
	@SuppressWarnings("unused")
	private final MapSettingsGuiController controller;

	private JPanel metaBox;

	private JPanel debugBox;

	private JComboBox<String> debugCheck;

	private JComboBox<String> debugCheats;

	private JComboBox<String> debugShell;

	private JComboBox<String> debugTrace;

	private JProsaTextField mapName;

	private JProsaTextField mapAuthor;

	private JProsaTextField mapVersion;

	private JPanel diplomacyBox;

	private JPanel colorBox;

	private JPanel resourceBox;

	private JPanel weatherBox;

	private JComboBox<String>[] playerColors;

	private JNumberTextField[] resources;

	private JComboBox<String> weatherSets;

	private JComboBox<String>[] playerDiplomacy;

	private JProsaTextField[] playerNames;

	/**
	 * Constructor
	 * 
	 * @param w Width
	 * @param h Height
	 * @param caption Caption of tab
	 * @param mapSettingsGuiController GUI controller
	 */
	public MapAssistantTab(
		final int w, final int h, final String caption, final MapSettingsGuiController mapSettingsGuiController
	)
	{
		super(w, h, caption);
		controller = mapSettingsGuiController;

		createMetaBox();
		createDebugBox();
		createDiplomacyBox();
		createColorBox();
		createResourceBox();
		createWeatherBox();

		setVisible(true);
	}

	/**
	 * Cleans all input fields from their contents.
	 */
	public void clean()
	{
		debugCheck.setSelectedIndex(0);
		debugCheats.setSelectedIndex(0);
		debugShell.setSelectedIndex(0);
		debugTrace.setSelectedIndex(0);
		weatherSets.setSelectedIndex(0);
		mapName.setText("");
		mapAuthor.setText("");
		mapVersion.setText("");
		
		for (int i=0; i<8; i++)
		{
			playerColors[i].setSelectedItem(0);
			playerDiplomacy[i].setSelectedItem(0);
			playerNames[i].setText("");
		}
		
		for (int i=0; i<6; i++)
		{
			resources[i].setText("0");
		}
	}

	/**
	 * Returns the text field with the map name.
	 * 
	 * @return Map name
	 */
	public JTextField getMapName()
	{
		return mapName;
	}

	/**
	 * Returns the text field with the map author.
	 * 
	 * @return Map author
	 */
	public JTextField getMapAuthor()
	{
		return mapAuthor;
	}

	/**
	 * Returns the text field with the map version.
	 * 
	 * @return Map version
	 */
	public JTextField getMapVersion()
	{
		return mapVersion;
	}

	/**
	 * Returns the checkbox for the quest check.
	 * 
	 * @return Quest check
	 */
	public JComboBox<String> getDebugCheck()
	{
		return debugCheck;
	}

	/**
	 * Returns the checkbox for the debug chets.
	 * 
	 * @return Debug cheats
	 */
	public JComboBox<String> getDebugCheats()
	{
		return debugCheats;
	}

	/**
	 * Returns the checkbox for the debug shell.
	 * 
	 * @return Debug shell
	 */
	public JComboBox<String> getDebugShell()
	{
		return debugShell;
	}

	/**
	 * Returns the checkbox for the quest trace.
	 * 
	 * @return Quest trace
	 */
	public JComboBox<String> getDebugTrace()
	{
		return debugTrace;
	}

	/**
	 * Returns the resource text field for the resource index
	 * 
	 * @param index Resource index
	 * @return Resource field
	 */
	public JTextField getResource(final int index)
	{
		return resources[index];
	}

	/**
	 * Returns the player name text field for the player ID.
	 * 
	 * @param player Player ID
	 * @return Player name
	 */
	public JTextField getPlayerName(final int player)
	{
		return playerNames[player - 1];
	}

	/**
	 * Sets the selected weather set in the weather combobox by the set name.
	 * 
	 * @param name Set name
	 */
	public void setSelectedWeatherSet(final String name)
	{
		for (int i = 0; i < weatherSets.getItemCount(); i++)
		{
			if (weatherSets.getItemAt(i).equals(name))
			{
				weatherSets.setSelectedIndex(i);
			}
		}
	}

	/**
	 * Returns the name of the current selected weather set.
	 * 
	 * @return Set name
	 */
	public String getSelectedWeatherSet()
	{
		return (String) weatherSets.getSelectedItem();
	}

	/**
	 * Sets the selected diplomacy state for the player.
	 * 
	 * @param player ID of player
	 * @param name Name of player
	 * @param state Diplomacy state
	 */
	public void setSelectedDiplomacy(final int player, final String name, final String state)
	{
		for (int i = 0; i < playerDiplomacy[player - 1].getItemCount(); i++)
		{
			if (playerDiplomacy[player - 1].getItemAt(i).equals(state))
			{
				playerDiplomacy[player - 1].setSelectedIndex(i);
			}
		}
		playerNames[player - 1].setText(name);
	}

	/**
	 * Returns the diplomatic relation of the human player to the AI player with the
	 * ID.
	 * 
	 * @param player Player ID
	 * @return Diplomatic relation
	 */
	public String getSelectedDiplomacy(final int player)
	{
		return (String) playerDiplomacy[player - 1].getSelectedItem();
	}

	/**
	 * Sets the selected player color for the player with the ID.
	 * 
	 * @param player Player ID
	 * @param name Color name
	 */
	public void setSelectedPlayerColor(final int player, final String name)
	{
		for (int i = 0; i < playerColors[player - 1].getItemCount(); i++)
		{
			if (playerColors[player - 1].getItemAt(i).equals(name))
			{
				playerColors[player - 1].setSelectedIndex(i);
			}
		}
	}

	/**
	 * Returns the selected player color for the player ID.
	 * 
	 * @param player Player ID
	 * @return Color name
	 */
	public String getSelectedPlayerColor(final int player)
	{
		return (String) playerColors[player - 1].getSelectedItem();
	}

	/**
	 * Returns the possible diplomacy states.
	 * 
	 * @return State names
	 */
	public String[] getDiplomacyNames()
	{
		final String[] diploNames = { "Neutral", "Hostile", "Friendly" };
		return diploNames;
	}

	/**
	 * Returns the possible player color names. Be careful:
	 * 
	 * <pre>
	 * DEFAULT_COLOR
	 * </pre>
	 * 
	 * is not a valid ingame player color. It will be interpreted to not set a color
	 * for the player.
	 * 
	 * @return Color names
	 */
	public String[] getColorNames()
	{
		final String[] colNames = { "DEFAULT_COLOR", "PLAYER_COLOR", "ENEMY_COLOR1", "ENEMY_COLOR2",
		"EVIL_GOVERNOR_COLOR", "FRIENDLY_COLOR1", "FRIENDLY_COLOR2", "FRIENDLY_COLOR3", "KERBEROS_COLOR",
		"NEPHILIM_COLOR", "NPC_COLOR", "ROBBERS_COLOR" };
		return colNames;
	}

	/**
	 * Returns the names of the weather sets. The names returned by this function
	 * are ingame gfx function names.
	 * 
	 * @return Set names
	 */
	public String[] getWeatherNames()
	{
		final String[] weatherNames = { "SetupEvelanceWeatherGfxSet", "SetupHighlandWeatherGfxSet",
		"SetupMediterraneanWeatherGfxSet", "SetupMoorWeatherGfxSet", "SetupNormalWeatherGfxSet",
		"SetupSteppeWeatherGfxSet" };
		return weatherNames;
	}

	/**
	 * Creates the weather box.
	 */
	private void createWeatherBox()
	{
		weatherBox = new JPanel(null);
		weatherBox.setBorder(BorderFactory.createTitledBorder("Wetter"));
		weatherBox.setBounds(5, h - 90, (int) (w * 0.5) - 10, 50);
		add(weatherBox);

		final int x = weatherBox.getWidth();
		final String[] weatherNames = getWeatherNames();

		weatherSets = new JComboBox<>();
		weatherSets.setBounds(10, 20, x - 20, 20);
		for (final String weatherName : weatherNames)
		{
			weatherSets.addItem(weatherName);
		}
		weatherBox.add(weatherSets);
	}

	/**
	 * Creates the diplomacy box.
	 */
	@SuppressWarnings("unchecked")
	private void createDiplomacyBox()
	{
		diplomacyBox = new JPanel(null);
		diplomacyBox.setBorder(BorderFactory.createTitledBorder("Diplomatie"));
		diplomacyBox.setBounds(5, 140, (int) (w * 0.5) - 10, h - 240);
		add(diplomacyBox);

		final int x = diplomacyBox.getWidth();
		final String[] diploNames = getDiplomacyNames();

		playerDiplomacy = new JComboBox[8];
		playerNames = new JProsaTextField[8];
		for (int i = 0; i < 8; i++)
		{
			playerDiplomacy[i] = new JComboBox<String>();
			playerDiplomacy[i].setBounds((int) (x * 0.6), 35 + (40 * i), (int) (x * 0.4) - 10, 20);
			for (final String diploName : diploNames)
			{
				playerDiplomacy[i].addItem(diploName);
			}
			playerDiplomacy[0].setEnabled(false);
			diplomacyBox.add(playerDiplomacy[i]);

			playerNames[i] = new JProsaTextField();
			playerNames[i].setBounds(10, 35 + (40 * i), (int) (x * 0.6) - 20, 20);
			playerNames[i].build();
			diplomacyBox.add(playerNames[i]);

			final JLabel label = new JLabel("Spieler " + (i + 1));
			label.setBounds(10, 20 + (40 * i), x - 20, 15);
			label.setFont(new Font(label.getName(), Font.PLAIN, 13));
			diplomacyBox.add(label);
		}
	}

	/**
	 * Creates the color box.
	 */
	@SuppressWarnings("unchecked")
	private void createColorBox()
	{
		colorBox = new JPanel(null);
		colorBox.setBorder(BorderFactory.createTitledBorder("Spielerfarben"));
		colorBox.setBounds((int) (w * 0.5) + 2, 170, (int) (w * 0.25) - 10, h - 210);
		add(colorBox);

		final int x = colorBox.getWidth();
		final String[] colNames = getColorNames();

		playerColors = new JComboBox[8];
		for (int i = 0; i < 8; i++)
		{
			playerColors[i] = new JComboBox<>();
			playerColors[i].setBounds(10, 38 + (40 * i), x - 20, 20);
			for (final String colName : colNames)
			{
				playerColors[i].addItem(colName);
			}
			colorBox.add(playerColors[i]);

			final JLabel label = new JLabel("Spieler " + (i + 1));
			label.setBounds(10, 23 + (40 * i), x - 20, 15);
			label.setFont(new Font(label.getName(), Font.PLAIN, 13));
			colorBox.add(label);
		}
	}

	/**
	 * Creates the resource box.
	 */
	private void createResourceBox()
	{
		resourceBox = new JPanel(null);
		resourceBox.setBorder(BorderFactory.createTitledBorder("Rohstoffe"));
		resourceBox.setBounds((int) (w - (w * 0.25)), 170, (int) (w * 0.25) - 10, h - 210);
		add(resourceBox);

		final int x = colorBox.getWidth();
		final String[] resNames = { "Taler", "Lehm", "Holz", "Stein", "Eisen", "Schwefel" };

		resources = new JNumberTextField[6];
		for (int i = 0; i < 6; i++)
		{
			resources[i] = new JNumberTextField();
			resources[i].setBounds(10, 38 + (40 * i), x - 20, 20);
			resources[i].setText("0");
			resources[i].build();
			resourceBox.add(resources[i]);

			final JLabel label = new JLabel(resNames[i]);
			label.setBounds(10, 23 + (40 * i), x - 20, 15);
			label.setFont(new Font(label.getName(), Font.PLAIN, 13));
			resourceBox.add(label);
		}
	}

	/**
	 * Creates the meta information box.
	 */
	private void createMetaBox()
	{
		metaBox = new JPanel(null);
		metaBox.setBorder(BorderFactory.createTitledBorder("Metadaten"));
		metaBox.setBounds(5, 10, (int) (w * 0.5) - 10, 120);
		add(metaBox);

		final int x = metaBox.getWidth();

		mapName = new JProsaTextField();
		mapName.setBounds(110, 25, x - 125, 20);
		mapName.build();
		metaBox.add(mapName);

		mapAuthor = new JProsaTextField();
		mapAuthor.setBounds(110, 55, x - 125, 20);
		mapAuthor.build();
		metaBox.add(mapAuthor);

		mapVersion = new JProsaTextField();
		mapVersion.setBounds(110, 85, x - 125, 20);
		mapVersion.build();
		metaBox.add(mapVersion);

		final JLabel mapNameLabel = new JLabel("Kartenname");
		mapNameLabel.setBounds(10, 25, 100, 15);
		mapNameLabel.setFont(new Font(mapNameLabel.getName(), Font.PLAIN, 13));
		metaBox.add(mapNameLabel);

		final JLabel mapAuthorLabel = new JLabel("Autor");
		mapAuthorLabel.setBounds(10, 55, 100, 15);
		mapAuthorLabel.setFont(new Font(mapAuthorLabel.getName(), Font.PLAIN, 13));
		metaBox.add(mapAuthorLabel);

		final JLabel mapVersionLabel = new JLabel("Version");
		mapVersionLabel.setBounds(10, 85, 100, 15);
		mapVersionLabel.setFont(new Font(mapVersionLabel.getName(), Font.PLAIN, 13));
		metaBox.add(mapVersionLabel);
	}

	/**
	 * Creates the debug box.
	 */
	private void createDebugBox()
	{
		debugBox = new JPanel(null);
		debugBox.setBorder(BorderFactory.createTitledBorder("Debug"));
		debugBox.setBounds((int) (w * 0.5), 10, (int) (w * 0.5) - 10, 150);
		add(debugBox);

		debugCheck = new JComboBox<>();
		debugCheck.setBounds(230, 25, 150, 20);
		debugCheck.addItem("true");
		debugCheck.addItem("false");
		debugBox.add(debugCheck);

		debugCheats = new JComboBox<>();
		debugCheats.setBounds(230, 55, 150, 20);
		debugCheats.addItem("true");
		debugCheats.addItem("false");
		debugBox.add(debugCheats);

		debugShell = new JComboBox<>();
		debugShell.setBounds(230, 85, 150, 20);
		debugShell.addItem("true");
		debugShell.addItem("false");
		debugBox.add(debugShell);

		debugTrace = new JComboBox<>();
		debugTrace.setBounds(230, 115, 150, 20);
		debugTrace.addItem("true");
		debugTrace.addItem("false");
		debugBox.add(debugTrace);

		final JLabel debugCheckLabel = new JLabel("Präventivprüfung aktivieren");
		debugCheckLabel.setBounds(10, 25, 180, 15);
		debugCheckLabel.setFont(new Font(debugCheckLabel.getName(), Font.PLAIN, 13));
		debugBox.add(debugCheckLabel);

		final JLabel debugCheatsLabel = new JLabel("Cheats aktivieren");
		debugCheatsLabel.setBounds(10, 55, 180, 15);
		debugCheatsLabel.setFont(new Font(debugCheatsLabel.getName(), Font.PLAIN, 13));
		debugBox.add(debugCheatsLabel);

		final JLabel mapVersionLabel = new JLabel("Shell aktivieren");
		mapVersionLabel.setBounds(10, 85, 180, 15);
		mapVersionLabel.setFont(new Font(mapVersionLabel.getName(), Font.PLAIN, 13));
		debugBox.add(mapVersionLabel);

		final JLabel debugTraceLabel = new JLabel("Questverfolgung aktivieren");
		debugTraceLabel.setBounds(10, 115, 180, 15);
		debugTraceLabel.setFont(new Font(mapVersionLabel.getName(), Font.PLAIN, 13));
		debugBox.add(debugTraceLabel);
	}

	/**
	 * Enables or disables all elements of the tab.
	 * 
	 * @param enabled Enabled flag
	 */
	public void enableTab(final boolean enabled)
	{
		mapName.setEnabled(enabled);
		mapAuthor.setEnabled(enabled);
		mapVersion.setEnabled(enabled);
		debugCheck.setEnabled(enabled);
		debugCheats.setEnabled(enabled);
		debugShell.setEnabled(enabled);
		debugTrace.setEnabled(enabled);
		weatherSets.setEnabled(enabled);

		for (int i = 1; i < 8; i++)
		{
			playerDiplomacy[i].setEnabled(enabled);
		}
		for (int i = 0; i < 8; i++)
		{
			playerNames[i].setEnabled(enabled);
		}
		for (int i = 0; i < 8; i++)
		{
			playerColors[i].setEnabled(enabled);
		}
		for (int i = 0; i < 6; i++)
		{
			resources[i].setEnabled(enabled);
		}
	}

	/**
	 * Main method for testing.
	 * 
	 * @param args Program arguments
	 */
	public static void main(final String[] args)
	{
		final JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setTitle("Siedler 5 Skriptassistent");
		f.setSize(810, 620);
		f.setLocationRelativeTo(null);
		f.setResizable(false);
		f.add(new MapAssistantTab(810, 620, "Foo", null));
		f.setVisible(true);
	}
}
