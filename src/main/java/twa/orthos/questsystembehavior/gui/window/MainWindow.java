
package twa.orthos.questsystembehavior.gui.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import twa.orthos.questsystembehavior.controller.MainWindowController;

/**
 * Class for the main window of the application.
 * 
 * @author totalwarANGEL
 */
@SuppressWarnings("serial")
public class MainWindow extends JPanel implements ActionListener
{
	private final int x;

	private final int y;

	private final MainWindowController interfaceController;

	private JButton openButton;

	private JButton saveButton;

	private JButton openMapButton;

	private JButton saveMapButton;

	private JLabel mapPathLabel;

	private JTextField mapPathField;

	private JTabbedPane tabs;

	/**
	 * Constructor
	 * 
	 * @param x Width
	 * @param y Height
	 * @param interfaceController Controller
	 */
	public MainWindow(final int x, final int y, final MainWindowController interfaceController)
	{
		this.interfaceController = interfaceController;
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns the button that triggers data loading.
	 * 
	 * @return Open button
	 */
	public JButton getOpenButton()
	{
		return openButton;
	}

	/**
	 * Returns the button that triggers data saving.
	 * 
	 * @return Save button
	 */
	public JButton getSaveButton()
	{
		return saveButton;
	}

	/**
	 * Returns the button that triggers map loading.
	 * 
	 * @return Open map button
	 */
	public JButton getOpenMapButton()
	{
		return openMapButton;
	}

	/**
	 * Returns the button that triggers map saving.
	 * 
	 * @return Save map button
	 */
	public JButton getSaveMapButton()
	{
		return saveMapButton;
	}

	/**
	 * Returns the text field with the path of the currently opened map.
	 * 
	 * @return Map path text field
	 */
	public JTextField getMapPathField()
	{
		return mapPathField;
	}

	/**
	 * Returns the tab container.
	 * 
	 * @return Tab container
	 */
	public JTabbedPane getTabPane()
	{
		return tabs;
	}

	/**
	 * Creates the main window.
	 */
	public void buildWindow()
	{
		setBounds(0, 0, x, y);
		setLayout(null);

		openButton = new JButton("Öffnen");
		openButton.setBounds(x - 270, y - 60, 120, 20);
		openButton.addActionListener(this);
		openButton.setEnabled(false);
		openButton.setVisible(true);
		add(openButton);

		saveButton = new JButton("Speichern");
		saveButton.setBounds(x - 140, y - 60, 120, 20);
		saveButton.addActionListener(this);
		saveButton.setEnabled(false);
		saveButton.setVisible(true);
		add(saveButton);

		openMapButton = new JButton("Map laden");
		openMapButton.setBounds(10, y - 60, 120, 20);
		openMapButton.addActionListener(this);
		openMapButton.setVisible(true);
		add(openMapButton);

		saveMapButton = new JButton("Map packen");
		saveMapButton.setBounds(140, y - 60, 120, 20);
		saveMapButton.addActionListener(this);
		saveMapButton.setEnabled(false);
		saveMapButton.setVisible(true);
		add(saveMapButton);

		mapPathLabel = new JLabel("Geladene Map:");
		mapPathLabel.setBounds(5, y - 100, x - 50, 15);
		mapPathLabel.setVisible(true);
		add(mapPathLabel);

		mapPathField = new JTextField();
		mapPathField.setBounds(5, y - 85, x - 15, 20);
		mapPathField.setEditable(false);
		mapPathField.setVisible(true);
		add(mapPathField);

		tabs = new JTabbedPane();
		tabs.setBounds(5, 5, x - 10, y - 115);
		tabs.setVisible(true);
		add(tabs);

		setVisible(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void actionPerformed(final ActionEvent e)
	{
		if (e.getSource() == openButton)
		{
			interfaceController.requestLoadSave();
		}
		if (e.getSource() == saveButton)
		{
			interfaceController.requestCreateSave();
		}
		if (e.getSource() == openMapButton)
		{
			interfaceController.requestLoadMap();
		}
		if (e.getSource() == saveMapButton)
		{
			interfaceController.requestSaveMap();
		}
	}
}