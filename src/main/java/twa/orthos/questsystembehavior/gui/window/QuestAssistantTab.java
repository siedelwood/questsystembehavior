
package twa.orthos.questsystembehavior.gui.window;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.NumberFormatter;
import twa.orthos.questsystembehavior.controller.QuestGuiController;
import twa.orthos.questsystembehavior.gui.swing.JNumberTextField;
import twa.orthos.questsystembehavior.gui.swing.JProsaTextField;
import twa.orthos.questsystembehavior.gui.swing.JRegexTextArea;

/**
 * This class implements the graphical user interface of the quest assistant
 * tab.
 * 
 * @author totalwarANGEL
 */
@SuppressWarnings("serial")
public class QuestAssistantTab extends AbstractTab implements ActionListener, ListSelectionListener, MouseListener
{

	private JPanel questBox;

	private JList<String> questList;

	private JButton addQuest;

	private JButton subQuest;

	private JPanel behaviorBox;

	private JList<String> behaviorList;

	private JButton addBehavior;

	private JButton subBehavior;

	private JPanel settingsBox;

	private JNumberTextField settingsTime;

	private JProsaTextField settingsTitle;

	private JRegexTextArea settingsText;

	private JComboBox<String> settingsPlayer;

	private JComboBox<String> settingsType;

	private JCheckBox settingsVisible;

	private JButton settingsUpdate;

	private final QuestGuiController controller;

	/**
	 * Constructor
	 * 
	 * @param w Width
	 * @param h Height
	 * @param caption Caption of tab
	 * @param questGuiController GUI controller
	 */
	public QuestAssistantTab(
		final int w, final int h, final String caption, final QuestGuiController questGuiController
	)
	{
		super(w, h, caption);
		controller = questGuiController;

		buildDetailsGroup(w, h);
		buildBehaviorGroup(w, h);
		buildQuestGroup(w, h);

		enableDetailsArea(false);
		enableBehaviorArea(false);
		enableQuestArea(false);
		setVisible(true);
	}

	/**
	 * Returns the list of quest names.
	 * 
	 * @return Quest names
	 */
	public JList<String> getQuestList()
	{
		return questList;
	}

	/**
	 * Returns the list of behavior names.
	 * 
	 * @return Behavior names
	 */
	public JList<String> getBehaviorList()
	{
		return behaviorList;
	}

	/**
	 * Returns the quest is visible checkbox.
	 * 
	 * @return Visible checkbox
	 */
	public JCheckBox getSettingsVisible()
	{
		return settingsVisible;
	}

	/**
	 * Returns the receiving player combobox.
	 * 
	 * @return Receiving player
	 */
	public JComboBox<String> getSettingsPlayer()
	{
		return settingsPlayer;
	}

	/**
	 * Returns the quest type combobox.
	 * 
	 * @return Quest type
	 */
	public JComboBox<String> getSettingsType()
	{
		return settingsType;
	}

	/**
	 * Returns the quest description text field.
	 * 
	 * @return Quest description
	 */
	public JTextArea getSettingsText()
	{
		return settingsText;
	}

	/**
	 * Returns the quest title text field.
	 * 
	 * @return Quest title
	 */
	public JTextField getSettingsTitle()
	{
		return settingsTitle;
	}

	/**
	 * Returns the quest finishing time text field.
	 * 
	 * @return Finishing time
	 */
	public JTextField getSettingsTime()
	{
		return settingsTime;
	}

	/**
	 * Cleans all input fields from their contents.
	 */
	public void clean()
	{
		questList.setListData(new Vector<>());
		behaviorList.setListData(new Vector<>());
		settingsVisible.setSelected(false);
		settingsPlayer.setSelectedIndex(0);
		settingsType.setSelectedIndex(0);
		settingsText.setText("");
		settingsTitle.setText("");
		settingsTime.setText("0");
	}

	/**
	 * Creates the details group.
	 * 
	 * @param x Width
	 * @param y Height
	 */
	public void buildDetailsGroup(final int x, final int y)
	{
		settingsBox = new JPanel(null);
		settingsBox.setBorder(BorderFactory.createTitledBorder("Auftragsparameter"));
		settingsBox.setBounds(260, 10, x - 525, y - 50);
		settingsBox.setVisible(true);
		add(settingsBox);

		settingsUpdate = new JButton("Update");
		settingsUpdate.setBounds((settingsBox.getWidth() / 2) - 50, settingsBox.getHeight() - 100, 100, 20);
		settingsUpdate.setVisible(true);
		settingsUpdate.addActionListener(this);
		settingsBox.add(settingsUpdate);

		final NumberFormat format = NumberFormat.getInstance();
		final DecimalFormat df = (DecimalFormat) format;
		df.applyPattern("#####");
		final NumberFormatter formatter = new NumberFormatter(df);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(0);
		formatter.setMaximum(Integer.MAX_VALUE);
		formatter.setAllowsInvalid(false);
		formatter.setCommitsOnValidEdit(false);

		settingsTime = new JNumberTextField();
		settingsTime.setBounds(x - 575, y - 390, 40, 20);
		settingsTime.build();
		settingsBox.add(settingsTime);

		settingsTitle = new JProsaTextField();
		settingsTitle.setBounds(10, y - 390, x - 600, 20);
		settingsTitle.build();
		settingsBox.add(settingsTitle);

		final JScrollPane scrollPaneDesc = new JScrollPane();
		settingsText = new JRegexTextArea("[^ \n0-9a-zäöüß@A-ZÄÖÜ.:\\-_!?'§\\$%/\\(\\)]");
		settingsText.setBounds(10, y - 360, x - 545, 200);
		settingsText.setLineWrap(true);
		settingsText.setWrapStyleWord(true);
		settingsText.build();
		scrollPaneDesc.setViewportView(settingsText);
		scrollPaneDesc.setBounds(10, y - 360, x - 545, 200);
		scrollPaneDesc.setVisible(true);
		settingsBox.add(scrollPaneDesc);

		settingsType = new JComboBox<>();
		settingsType.addItem("MAINQUEST_OPEN");
		settingsType.addItem("SUBQUEST_OPEN");
		settingsType.setBounds(10, 90, x - 545, 20);
		settingsType.addActionListener(this);
		settingsBox.add(settingsType);

		settingsPlayer = new JComboBox<>();
		for (int i = 1; i < 9; i++)
		{
			settingsPlayer.addItem("" + i);
		}
		settingsPlayer.setBounds(10, 140, x - 545, 20);
		settingsPlayer.addActionListener(this);
		settingsBox.add(settingsPlayer);

		settingsVisible = new JCheckBox("Quest anzeigen");
		settingsVisible.setFont(new Font(settingsVisible.getName(), Font.PLAIN, 13));
		settingsVisible.setBounds(8, 25, x - 545, 20);
		settingsVisible.addActionListener(this);
		settingsBox.add(settingsVisible);

		final JLabel settingsTimeLabel = new JLabel("Zeit");
		settingsTimeLabel.setFont(new Font(settingsTimeLabel.getName(), Font.PLAIN, 13));
		settingsTimeLabel.setBounds(x - 570, y - 410, 40, 20);
		settingsBox.add(settingsTimeLabel);

		final JLabel settingsTitleLabel = new JLabel("Auftragsbeschreibung");
		settingsTitleLabel.setFont(new Font(settingsTitleLabel.getName(), Font.PLAIN, 13));
		settingsTitleLabel.setBounds(10, y - 410, 200, 20);
		settingsBox.add(settingsTitleLabel);

		final JLabel settingsTypeLabel = new JLabel("Auftragstyp");
		settingsTypeLabel.setFont(new Font(settingsTypeLabel.getName(), Font.PLAIN, 13));
		settingsTypeLabel.setBounds(10, 70, 200, 20);
		settingsBox.add(settingsTypeLabel);

		final JLabel settingsPlayerLabel = new JLabel("Auftragnehmer");
		settingsPlayerLabel.setFont(new Font(settingsPlayerLabel.getName(), Font.PLAIN, 13));
		settingsPlayerLabel.setBounds(10, 120, 200, 20);
		settingsBox.add(settingsPlayerLabel);
	}

	/**
	 * Creates the behavior group.
	 * 
	 * @param x Width
	 * @param y Height
	 */
	public void buildBehaviorGroup(final int x, final int y)
	{
		behaviorBox = new JPanel(null);
		behaviorBox.setBorder(BorderFactory.createTitledBorder("Verhaltensliste"));
		behaviorBox.setBounds(x - 260, 10, 250, y - 50);
		behaviorBox.setVisible(true);
		add(behaviorBox);

		addBehavior = new JButton("+");
		addBehavior.setBounds(75, y - 85, 45, 25);
		addBehavior.addActionListener(this);
		behaviorBox.add(addBehavior);

		subBehavior = new JButton("-");
		subBehavior.setBounds(140, y - 85, 45, 25);
		subBehavior.addActionListener(this);
		behaviorBox.add(subBehavior);

		final JScrollPane scrollPaneForbid = new JScrollPane();
		behaviorList = new JList<>();
		behaviorList.setBounds(10, 25, 230, y - 120);
		behaviorList.addListSelectionListener(this);
		behaviorList.addMouseListener(this);
		scrollPaneForbid.setViewportView(behaviorList);
		scrollPaneForbid.setBounds(10, 25, 230, y - 120);
		scrollPaneForbid.setVisible(true);
		behaviorBox.add(scrollPaneForbid);
	}

	/**
	 * Creates the quest group.
	 * 
	 * @param x Width
	 * @param y Height
	 */
	public void buildQuestGroup(final int x, final int y)
	{
		questBox = new JPanel(null);
		questBox.setBorder(BorderFactory.createTitledBorder("Auftragsliste"));
		questBox.setBounds(5, 10, 250, y - 50);
		questBox.setVisible(true);
		add(questBox);

		addQuest = new JButton("+");
		addQuest.setBounds(75, y - 85, 45, 25);
		addQuest.addActionListener(this);
		questBox.add(addQuest);

		subQuest = new JButton("-");
		subQuest.setBounds(140, y - 85, 45, 25);
		subQuest.addActionListener(this);
		questBox.add(subQuest);

		final JScrollPane scrollPaneForbid = new JScrollPane();
		questList = new JList<>();
		questList.setBounds(10, 25, 230, y - 120);
		questList.addListSelectionListener(this);
		scrollPaneForbid.setViewportView(questList);
		scrollPaneForbid.setBounds(10, 25, 230, y - 120);
		scrollPaneForbid.setVisible(true);
		questBox.add(scrollPaneForbid);
	}

	/**
	 * Enables or disables the details area in the main window.
	 * 
	 * @param enabled Area enabled
	 */
	public void enableDetailsArea(final boolean enabled)
	{
		settingsTime.setEnabled(enabled);
		settingsTitle.setEnabled(enabled);
		settingsText.setEnabled(enabled);
		settingsType.setEnabled(enabled);
		settingsPlayer.setEnabled(enabled);
		settingsVisible.setEnabled(enabled);
		settingsUpdate.setEnabled(enabled);
	}

	/**
	 * Enables or disables the behavior area in the main window.
	 * 
	 * @param enabled Area enabled
	 */
	public void enableBehaviorArea(final boolean enabled)
	{
		behaviorList.setEnabled(enabled);
		addBehavior.setEnabled(enabled);
		subBehavior.setEnabled(enabled);
	}

	/**
	 * Enables or disables the quest area in the main window.
	 * 
	 * @param enabled Area enabled
	 */
	public void enableQuestArea(final boolean enabled)
	{
		questList.setEnabled(enabled);
		addQuest.setEnabled(enabled);
		subQuest.setEnabled(enabled);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void actionPerformed(final ActionEvent e)
	{
		if (e.getSource() == addBehavior)
		{
			controller.addBehavior();
		}
		if (e.getSource() == subBehavior)
		{
			controller.removeBehavior();
		}
		if (e.getSource() == addQuest)
		{
			controller.addQuest();
		}
		if (e.getSource() == subQuest)
		{
			controller.removeQuest();
		}
		if (e.getSource() == settingsUpdate)
		{
			controller.updateDisplayedQuest();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void valueChanged(final ListSelectionEvent e)
	{
		if (e.getSource() == questList)
		{
			controller.onQuestSelected(questList.getSelectedIndex());
		}
		if (e.getSource() == behaviorList)
		{
			controller.onBehaviorSelected(behaviorList.getSelectedIndex());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseClicked(final MouseEvent e)
	{
		if (e.getSource() == behaviorList)
		{
			if (e.getClickCount() == 2)
			{
				controller.onBehaviorDoubleClicked(behaviorList.getSelectedIndex());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mousePressed(final MouseEvent e)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseReleased(final MouseEvent e)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseEntered(final MouseEvent e)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseExited(final MouseEvent e)
	{
	}

	/**
	 * Main method for testing.
	 * 
	 * @param args Program arguments
	 */
	public static void main(final String[] args)
	{
		final JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setTitle("Siedler 5 Skriptassistent");
		f.setSize(810, 670);
		f.setLocationRelativeTo(null);
		f.setResizable(false);
		f.add(new QuestAssistantTab(810, 670, "Foo", null));
		f.setVisible(true);
	}
}
