
package twa.orthos.questsystembehavior.gui.window;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import twa.orthos.questsystembehavior.controller.BriefingGuiController;
import twa.orthos.questsystembehavior.gui.swing.JProsaTextField;
import twa.orthos.questsystembehavior.gui.tree.TreeUserDataRoot;

/**
 * This class implements the graphical user interface of the briefing assistant
 * tab.
 * 
 * @author totalwarANGEL
 */
@SuppressWarnings("serial")
public class BriefingAssistantTab extends AbstractTab implements ActionListener, TreeSelectionListener
{
	public static int LEVEL_ROOT = 0;

	public static int LEVEL_BRIEFING = 1;

	public static int LEVEL_PAGE = 2;

	private final BriefingGuiController controller;

	private JTree tree;

	private JButton addBriefing;

	private JButton subBriefing;

	private JPanel briefingBox;

	private JPanel rootBox;

	private JButton subPage;

	private JButton addPage;

	private JButton savePage;

	private JPanel branchBox;

	private JPanel leaveBox;

	private JTextArea pageText;

	private JProsaTextField pageTitle;

	private JProsaTextField pagePosition;

	private JProsaTextField pageAction;

	private JComboBox<String> pageDialog;

	private DefaultMutableTreeNode briefingTreeTop;

	private JComboBox<String> displaySky;

	private JComboBox<String> hideFogOfWar;

	private JScrollPane treeView;

	/**
	 * Returns the actual tree.
	 * 
	 * @return Briefing tree
	 */
	public JTree getBriefingTree()
	{
		return tree;
	}

	/**
	 * Returns the tree view of the briefing list.
	 * 
	 * @return Briefing tree view
	 */
	public JScrollPane getBriefingTreeView()
	{
		return treeView;
	}

	/**
	 * Returns the add briefing button.
	 * 
	 * @return Add briefing button
	 */
	public JButton getAddBriefing()
	{
		return addBriefing;
	}

	/**
	 * Returns the remove briefing button.
	 * 
	 * @return Remove briefing button
	 */
	public JButton getSubBriefing()
	{
		return subBriefing;
	}

	/**
	 * Returns the remove page button.
	 * 
	 * @return Remove page button
	 */
	public JButton getSubPage()
	{
		return subPage;
	}

	/**
	 * Returns the add page button.
	 * 
	 * @return Add page button
	 */
	public JButton getAdPage()
	{
		return addPage;
	}

	/**
	 * Returns the save page button.
	 * 
	 * @return Save page button
	 */
	public JButton getSavePage()
	{
		return savePage;
	}

	/**
	 * Returns the text field with the page text.
	 * 
	 * @return Page text
	 */
	public JTextArea getPageText()
	{
		return pageText;
	}

	/**
	 * Returns the text field with the page title.
	 * 
	 * @return Page title
	 */
	public JTextField getPageTitle()
	{
		return pageTitle;
	}

	/**
	 * Returns the text field with the page position.
	 * 
	 * @return Page position
	 */
	public JTextField getPagePosition()
	{
		return pagePosition;
	}

	/**
	 * Returns the text field with the page action.
	 * 
	 * @return Page action
	 */
	public JTextField getPageAction()
	{
		return pageAction;
	}

	/**
	 * Returns the combobox with the dialog page booleans.
	 * 
	 * @return Dialog page
	 */
	public JComboBox<String> getPageDialog()
	{
		return pageDialog;
	}

	/**
	 * Returns the combobox with the display sky booleans.
	 * 
	 * @return Display sky
	 */
	public JComboBox<String> getDisplaySky()
	{
		return displaySky;
	}

	/**
	 * Returns the combobox with the fog of war booleans.
	 * 
	 * @return Fog of war
	 */
	public JComboBox<String> getHideFogOfWar()
	{
		return hideFogOfWar;
	}

	/**
	 * Returns the top node of the briefing tree.
	 * 
	 * @return Top node
	 */
	public DefaultMutableTreeNode getBriefingTopNode()
	{
		return briefingTreeTop;
	}

	/**
	 * Sets the top node of the briefing tree.
	 * 
	 * @param top Top node
	 */
	public void setBriefingTopNode(final DefaultMutableTreeNode top)
	{
		briefingTreeTop = top;
	}

	/**
	 * Returns the properties container of the root node.
	 * 
	 * @return Root properties box
	 */
	public JPanel getRootBox()
	{
		return rootBox;
	}

	/**
	 * Returns the properties container of a briefing node.
	 * 
	 * @return Briefing properties box
	 */
	public JPanel getBranchBox()
	{
		return branchBox;
	}

	/**
	 * Returns the properties container of a page node.
	 * 
	 * @return Page properties box
	 */
	public JPanel getLeaveBox()
	{
		return leaveBox;
	}

	/**
	 * Constructor
	 * 
	 * @param w Width
	 * @param h Height
	 * @param caption Caption of tab
	 * @param briefingGuiController Controller
	 */
	public BriefingAssistantTab(
		final int w, final int h, final String caption, final BriefingGuiController briefingGuiController
	)
	{
		super(w, h, caption);
		controller = briefingGuiController;
		this.w = w;
		this.h = h;

		briefingTreeTop = new DefaultMutableTreeNode(new TreeUserDataRoot("Briefings"));
		createTreeViewBox();
		createRootBox();
		createBranchBox();
		createLeaveBox();

		setVisible(true);
	}

	/**
	 * Cleans all input fields from their contents.
	 */
	public void clean()
	{
		briefingTreeTop = new DefaultMutableTreeNode(new TreeUserDataRoot("Briefings"));
		((DefaultTreeModel) tree.getModel()).setRoot(briefingTreeTop);
	}

	/**
	 * Enables or disables the properties view.
	 * 
	 * @param enabled Enabled flag
	 */
	public void enablePropertiesView(final boolean enabled)
	{
		addBriefing.setEnabled(enabled);

		subBriefing.setEnabled(enabled);
		addPage.setEnabled(enabled);

		subPage.setEnabled(enabled);
		savePage.setEnabled(enabled);
		pageText.setEnabled(enabled);
		pageTitle.setEnabled(enabled);
		pagePosition.setEnabled(enabled);
		pageAction.setEnabled(enabled);
		pageDialog.setEnabled(enabled);
		displaySky.setEnabled(enabled);
		hideFogOfWar.setEnabled(enabled);
	}

	/**
	 * Enables or disables the tree view.
	 * 
	 * @param enabled Enabled flag
	 */
	public void enableTreeView(final boolean enabled)
	{
		tree.setEnabled(enabled);
	}

	/**
	 * Creates the properties container of the root node.
	 */
	private void createRootBox()
	{
		rootBox = new JPanel(null);
		rootBox.setBorder(BorderFactory.createTitledBorder("Eigenschaften"));
		rootBox.setBounds((int) (w * 0.4) + 5, 10, (int) (w * 0.6) - 15, h - 50);
		add(rootBox);

		addBriefing = new JButton("Briefing erstellen");
		addBriefing.setBounds(10, 25, 180, 25);
		addBriefing.addActionListener(this);
		rootBox.add(addBriefing);

		rootBox.setVisible(false);
	}

	/**
	 * Creates the properties container of the briefing nodes.
	 */
	private void createBranchBox()
	{
		branchBox = new JPanel(null);
		branchBox.setBorder(BorderFactory.createTitledBorder("Eigenschaften"));
		branchBox.setBounds((int) (w * 0.4) + 5, 10, (int) (w * 0.6) - 15, h - 50);
		add(branchBox);

		final int y = branchBox.getHeight();

		addPage = new JButton("Seite erstellen");
		addPage.setBounds(10, 25, 180, 25);
		addPage.addActionListener(this);
		branchBox.add(addPage);

		subBriefing = new JButton("Briefing löschen");
		subBriefing.setBounds(10, y - 37, 180, 25);
		subBriefing.addActionListener(this);
		branchBox.add(subBriefing);

		branchBox.setVisible(false);
	}

	/**
	 * Creates the properties container of the page nodes.
	 */
	private void createLeaveBox()
	{
		leaveBox = new JPanel(null);
		leaveBox.setBorder(BorderFactory.createTitledBorder("Eigenschaften"));
		leaveBox.setBounds((int) (w * 0.4) + 5, 10, (int) (w * 0.6) - 15, h - 50);
		add(leaveBox);

		final int x = leaveBox.getWidth();
		final int y = leaveBox.getHeight();

		savePage = new JButton("Seite speichern");
		savePage.setBounds(x - 190, y - 37, 180, 25);
		savePage.addActionListener(this);
		leaveBox.add(savePage);

		subPage = new JButton("Seite löschen");
		subPage.setBounds(10, y - 37, 180, 25);
		subPage.addActionListener(this);
		leaveBox.add(subPage);

		final JScrollPane pageTextScroll = new JScrollPane();
		pageText = new JTextArea();
		pageText.setLineWrap(true);
		pageText.setWrapStyleWord(true);
		pageTextScroll.setViewportView(pageText);
		pageTextScroll.setBounds((int) (x * 0.4 + 10), 95, (int) (x * 0.6) - 20, 100);
		pageTextScroll.setVisible(true);
		leaveBox.add(pageTextScroll);

		pageTitle = new JProsaTextField();
		pageTitle.setBounds((int) (x * 0.4 + 10), 60, (int) (x * 0.6 - 20), 20);
		pageTitle.build();
		leaveBox.add(pageTitle);

		pagePosition = new JProsaTextField();
		pagePosition.setBounds((int) (x * 0.4 + 10), 30, (int) (x * 0.6 - 20), 20);
		pagePosition.build();
		leaveBox.add(pagePosition);

		pageAction = new JProsaTextField();
		pageAction.setBounds((int) (x * 0.4 + 10), 330, (int) (x * 0.6 - 20), 20);
		pageAction.build();
		leaveBox.add(pageAction);

		pageDialog = new JComboBox<String>();
		pageDialog.setBounds((int) (x * 0.4 + 10), 215, (int) (x * 0.6 - 20), 20);
		pageDialog.addItem("true");
		pageDialog.addItem("false");
		leaveBox.add(pageDialog);

		displaySky = new JComboBox<String>();
		displaySky.setBounds((int) (x * 0.4 + 10), 290, (int) (x * 0.6 - 20), 20);
		displaySky.addItem("true");
		displaySky.addItem("false");
		leaveBox.add(displaySky);

		hideFogOfWar = new JComboBox<String>();
		hideFogOfWar.setBounds((int) (x * 0.4 + 10), 250, (int) (x * 0.6 - 20), 20);
		hideFogOfWar.addItem("true");
		hideFogOfWar.addItem("false");
		leaveBox.add(hideFogOfWar);

		final JLabel pageTextLabel = new JLabel("Text der Seite");
		pageTextLabel.setBounds(10, 95, (int) (x * 0.4 - 20), 15);
		pageTextLabel.setFont(new Font(pageTextLabel.getName(), Font.PLAIN, 13));
		leaveBox.add(pageTextLabel);

		final JLabel pageTitleLabel = new JLabel("Titel der Seite");
		pageTitleLabel.setBounds(10, 60, (int) (x * 0.4 - 20), 15);
		pageTitleLabel.setFont(new Font(pageTitleLabel.getName(), Font.PLAIN, 13));
		leaveBox.add(pageTitleLabel);

		final JLabel pagePositionLabel = new JLabel("Kameraposition");
		pagePositionLabel.setBounds(10, 30, (int) (x * 0.4 - 20), 15);
		pagePositionLabel.setFont(new Font(pageTitleLabel.getName(), Font.PLAIN, 13));
		leaveBox.add(pagePositionLabel);

		final JLabel pageDialogLabel = new JLabel("Dialogsicht nutzen");
		pageDialogLabel.setBounds(10, 215, (int) (x * 0.4 - 20), 15);
		pageDialogLabel.setFont(new Font(pageDialogLabel.getName(), Font.PLAIN, 13));
		leaveBox.add(pageDialogLabel);

		final JLabel pageActionLabel = new JLabel("Funktion aufrufen");
		pageActionLabel.setBounds(10, 330, (int) (x * 0.4 - 20), 15);
		pageActionLabel.setFont(new Font(pageActionLabel.getName(), Font.PLAIN, 13));
		leaveBox.add(pageActionLabel);

		final JLabel pageSkyLabel = new JLabel("Himmel einblenden");
		pageSkyLabel.setBounds(10, 290, (int) (x * 0.4 - 20), 15);
		pageSkyLabel.setFont(new Font(pageSkyLabel.getName(), Font.PLAIN, 13));
		leaveBox.add(pageSkyLabel);

		final JLabel pageFogLabel = new JLabel("Fog of War deaktivieren");
		pageFogLabel.setBounds(10, 250, (int) (x * 0.4 - 20), 15);
		pageFogLabel.setFont(new Font(pageFogLabel.getName(), Font.PLAIN, 13));
		leaveBox.add(pageFogLabel);

		leaveBox.setVisible(false);
	}

	/**
	 * Creates the tree view container.
	 */
	public void createTreeViewBox()
	{
		if (briefingBox != null)
		{
			remove(briefingBox);
		}
		briefingBox = new JPanel(null);
		briefingBox.setBorder(BorderFactory.createTitledBorder("Dialogbaum"));
		briefingBox.setBounds(5, 10, (int) (w * 0.4) - 5, h - 50);
		briefingBox.setVisible(true);
		add(briefingBox);

		tree = new JTree(briefingTreeTop);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.addTreeSelectionListener(this);
		treeView = new JScrollPane(tree);
		treeView.setBounds(10, 20, (int) (w * 0.4) - 25, h - 80);
		briefingBox.add(treeView);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void actionPerformed(final ActionEvent e)
	{
		final DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

		if (e.getSource() == addPage)
		{
			controller.addBriefingPage(selectedNode);
		}

		if (e.getSource() == subPage)
		{
			controller.removeBriefingPage(selectedNode);
		}

		if (e.getSource() == savePage)
		{
			controller.saveCurrentBriefingPage(selectedNode);
		}

		if (e.getSource() == addBriefing)
		{
			controller.addBriefing();
		}

		if (e.getSource() == subBriefing)
		{
			controller.removeBriefing(selectedNode);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void valueChanged(final TreeSelectionEvent e)
	{
		if (e.getSource() == tree)
		{
			final DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
			if (selectedNode != null)
			{
				if (selectedNode.getLevel() == LEVEL_ROOT)
				{
					controller.displayTreeRoot();
				}

				if (selectedNode.getLevel() == LEVEL_BRIEFING)
				{
					controller.displayTreeBranch();
				}

				if (selectedNode.getLevel() == LEVEL_PAGE)
				{
					controller.displayTreeLeave(selectedNode);
				}
			}
		}
	}

	/**
	 * Main method for testing
	 * 
	 * @param args Program arguments
	 */
	public static void main(final String[] args)
	{
		final JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setTitle("Siedler 5 Skriptassistent");
		f.setSize(810, 620);
		f.setLocationRelativeTo(null);
		f.setResizable(false);
		f.add(new BriefingAssistantTab(810, 620, "Foo", null));
		f.setVisible(true);
	}
}
