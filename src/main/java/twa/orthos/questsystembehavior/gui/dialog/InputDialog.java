
package twa.orthos.questsystembehavior.gui.dialog;

import java.awt.event.ActionListener;

/**
 * Interface for input dialogs that hides all public methods of it's
 * implementations.
 * 
 * @author totalwarANGEL
 *
 */
public interface InputDialog extends ActionListener
{
}
