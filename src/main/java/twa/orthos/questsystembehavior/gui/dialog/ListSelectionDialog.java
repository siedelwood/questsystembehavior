
package twa.orthos.questsystembehavior.gui.dialog;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/**
 * Implementation of an input dialog that requests the selection of an item.
 * 
 * @author totalwarANGEL
 *
 */
@SuppressWarnings("serial")
public class ListSelectionDialog extends JDialog implements InputDialog
{
	protected JList<String> elementList;

	protected JButton confirm;

	protected JFrame parent;

	protected int listIndex;

	protected int h;

	protected Vector<String> dataList;

	/**
	 * Constructor
	 * 
	 * @param h window height
	 * @param title Title of window
	 * @param parent Parent frame
	 */
	public ListSelectionDialog(final int h, final String title, final JFrame parent)
	{
		super(parent, title, true);
		dataList = new Vector<>();
		this.h = h;
		listIndex = -1;
	}

	/**
	 * Sets the list data of the selection dialog.
	 * 
	 * @param dataList Data list
	 */
	public void setData(final Vector<String> dataList)
	{
		this.dataList = dataList;
		if (elementList != null)
		{
			elementList.setListData(dataList);
		}
	}

	/**
	 * Builds the window.
	 */
	public void build()
	{
		final int w = 326;

		setSize(w, h);
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);

		final JPanel panel = new JPanel(null);
		panel.setSize(w, h);
		add(panel);

		final JScrollPane scrollPane = new JScrollPane();
		elementList = new JList<String>();
		elementList.setListData(dataList);
		elementList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(elementList);
		scrollPane.setBounds(10, 10, w - 20, h - 80);
		scrollPane.setVisible(true);
		panel.add(scrollPane);

		confirm = new JButton("Auswählen");
		confirm.setBounds((w / 2) - 60, h - 65, 120, 20);
		confirm.addActionListener(this);
		confirm.setVisible(true);
		panel.add(confirm);

		add(panel);
		getRootPane().setDefaultButton(confirm);
		setVisible(true);
	}

	/**
	 * Resets the dialog and the selection list.
	 */
	public void reset()
	{
		if (dataList.size() > 0)
		{
			elementList.setSelectedIndex(0);
		}
		listIndex = -1;
	}

	/**
	 * Returns the selected text from the list.
	 * 
	 * @return Selected text
	 */
	public String getSelected()
	{
		if (listIndex != -1)
		{
			return dataList.get(listIndex);
		}
		return null;
	}

	/**
	 * Checks if the user has accepted the changes in the dialog.
	 * 
	 * @param e Action event
	 */
	@Override
	public void actionPerformed(final ActionEvent e)
	{
		if (e.getSource() == confirm)
		{
			listIndex = elementList.getSelectedIndex();
		}
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	/**
	 * Main method for testing.
	 * 
	 * @param args Program arguments
	 */
	public static void main(final String[] args)
	{
		final Vector<String> dataList = new Vector<>();
		dataList.add("Goal_SomeTest1");
		dataList.add("Goal_SomeTest2");
		dataList.add("Reward_SomeTest1");
		dataList.add("Reprisal_SomeTest1");
		dataList.add("Trigger_SomeTest1");
		dataList.add("Trigger_SomeTest2");

		final ListSelectionDialog sbd = new ListSelectionDialog(600, "Input", null);
		sbd.setData(dataList);
		sbd.build();

		System.out.println(sbd.getSelected());
	}
}
