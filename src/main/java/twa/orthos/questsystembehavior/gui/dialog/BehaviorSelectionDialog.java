
package twa.orthos.questsystembehavior.gui.dialog;

import java.awt.Font;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Extension of the list selection to implement a behavior selection dialog.
 * 
 * @author totalwarANGEL
 *
 */
@SuppressWarnings("serial")
public class BehaviorSelectionDialog extends ListSelectionDialog implements ListSelectionListener
{
	protected JLabel behaviorDescription;

	protected Vector<String> behaviorDescriptionList;

	/**
	 * Constructor
	 * 
	 * @param h window height
	 * @param title Title of window
	 * @param parent Parent frame
	 */
	public BehaviorSelectionDialog(final int h, final String title, final JFrame parent)
	{
		super(h, title, parent);
		behaviorDescriptionList = new Vector<>();
	}

	/**
	 * Sets the contents of the behavior list.
	 * 
	 * @param dataList Elements to add
	 */
	public void setDescription(final Vector<String> dataList)
	{
		behaviorDescriptionList = dataList;
		if (behaviorDescriptionList != null)
		{
			behaviorDescriptionList.addAll(dataList);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void build()
	{
		final int w = 326;

		setSize(w, h);
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);

		final JPanel panel = new JPanel(null);
		panel.setSize(w, h);
		add(panel);

		final JScrollPane scrollPane = new JScrollPane();
		elementList = new JList<String>();
		elementList.setListData(dataList);
		elementList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		elementList.addListSelectionListener(this);
		scrollPane.setViewportView(elementList);
		scrollPane.setBounds(10, 10, w - 20, h - 230);
		scrollPane.setVisible(true);
		panel.add(scrollPane);

		behaviorDescription = new JLabel("");
		behaviorDescription.setVerticalAlignment(SwingConstants.TOP);
		behaviorDescription.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		behaviorDescription.setBounds(10, h - 220, w - 20, 130);
		panel.add(behaviorDescription);

		confirm = new JButton("Auswählen");
		confirm.setBounds((w / 2) - 60, h - 65, 120, 20);
		confirm.addActionListener(this);
		confirm.setVisible(true);
		panel.add(confirm);

		add(panel);
		getRootPane().setDefaultButton(confirm);
		setVisible(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void valueChanged(final ListSelectionEvent e)
	{
		if (e.getSource() == elementList)
		{
			final int selected = elementList.getSelectedIndex();
			if (selected > -1 && behaviorDescriptionList.size() > selected)
			{
				final StringBuilder text = new StringBuilder();
				text.append("<html><b>Beschreibung:</b><br>");
				text.append(behaviorDescriptionList.get(selected));
				text.append("</html>");
				behaviorDescription.setText(text.toString());
			}
		}
	}
}
