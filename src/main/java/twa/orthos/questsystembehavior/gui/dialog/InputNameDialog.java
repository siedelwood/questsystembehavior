
package twa.orthos.questsystembehavior.gui.dialog;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import twa.orthos.questsystembehavior.gui.swing.JScriptSaveTextField;

/**
 * Input dialog implementation for requesting an name.
 * 
 * @author totalwarANGEL
 *
 */
@SuppressWarnings("serial")
public class InputNameDialog extends JDialog implements InputDialog
{
	protected int w;

	protected int h;

	protected JFrame parent;

	protected String caption;

	protected JScriptSaveTextField name;

	protected String givenName;

	protected JButton accept;

	/**
	 * Returns the selected name.
	 * 
	 * @return Name
	 */
	public String getGivenName()
	{
		return givenName;
	}

	/**
	 * Returns true, if the changes had been accepted.
	 * 
	 * @return Changes accepted
	 */
	public boolean hasAccepted()
	{
		return givenName != null && !givenName.equals("");
	}

	/**
	 * Constructor
	 * 
	 * @param w Window with
	 * @param caption Title of window
	 * @param parent Parent frame
	 */
	public InputNameDialog(final int w, final String caption, final JFrame parent)
	{
		super(parent, caption, true);
		this.parent = parent;
		this.caption = caption;
		this.w = w;
		h = 100;
		build();
	}

	/**
	 * Resets the dialog and the text field.
	 */
	public void reset()
	{
		name.setText("");
		givenName = null;
	}

	/**
	 * Builds the window.
	 */
	private void build()
	{
		setSize(w, h);
		setResizable(false);
		setLayout(null);
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle(caption);

		accept = new JButton("Auswählen");
		accept.setBounds((w / 2) - 60, h - 60, 120, 20);
		accept.addActionListener(this);
		add(accept);

		name = new JScriptSaveTextField("");
		name.setBounds(10, 15, w - 20, 22);
		name.build();
		add(name);

		getRootPane().setDefaultButton(accept);
		setVisible(true);
	}

	/**
	 * Checks if the user has accepted the changes in the dialog.
	 * 
	 * @param e Action event
	 */
	@Override
	public void actionPerformed(final ActionEvent e)
	{
		if (e.getSource() == accept)
		{
			givenName = name.getText();
		}
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	/**
	 * Main method for testing.
	 * 
	 * @param args Program arguments
	 */
	public static void main(final String[] args)
	{
		final InputNameDialog id = new InputNameDialog(350, "Input", null);
		System.out.println(id.getGivenName());
	}
}
