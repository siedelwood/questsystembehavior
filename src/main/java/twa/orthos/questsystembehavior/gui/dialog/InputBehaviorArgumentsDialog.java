
package twa.orthos.questsystembehavior.gui.dialog;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import twa.orthos.questsystembehavior.gui.input.InputComponent;
import twa.orthos.questsystembehavior.gui.input.InputComponentFactory;
import twa.orthos.questsystembehavior.gui.input.InputComponentFactoryImpl;

/**
 * Implementation of an input dialog for behavior arguments.
 * 
 * @author totalwarANGEL
 *
 */
@SuppressWarnings("serial")
public class InputBehaviorArgumentsDialog extends JDialog implements InputDialog
{
	protected int w;

	protected int h;

	protected JFrame parent;

	protected String caption;

	protected List<InputComponent> fields;

	protected JButton accept;

	protected boolean accepted = false;

	protected JButton decline;

	/**
	 * Constructor
	 * 
	 * @param w Width of Dialog
	 * @param caption Title of dialog
	 * @param parent parent component
	 */
	public InputBehaviorArgumentsDialog(final int w, final String caption, final JFrame parent)
	{
		fields = new ArrayList<>();
		this.parent = parent;
		this.caption = caption;
		this.w = w;
		h = 70;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> toList()
	{
		final List<String> list = new ArrayList<>();
		list.add(caption);
		list.addAll(getValues());
		return list;
	}

	/**
	 * Sets the fields displayed by the dialog.
	 * 
	 * @param fields Field components
	 */
	public void setFields(final List<InputComponent> fields)
	{
		this.fields = fields;
	}

	/**
	 * Returns a list of all fields in the dialog.
	 * 
	 * @return List of fields
	 */
	public List<InputComponent> getFields()
	{
		return fields;
	}

	/**
	 * Returns the values of all fields of the dialog.
	 * 
	 * @return List of values
	 */
	public List<String> getValues()
	{
		final List<String> values = new ArrayList<>();
		for (int i = 0; i < fields.size(); i++)
		{
			if (fields.get(i) != null)
			{
				values.add(fields.get(i).getValue());
			}
			else
			{
				values.add("NULL");
			}
		}
		return values;
	}

	/**
	 * Returns true, if the player has accepted the changes.
	 * 
	 * @return
	 */
	public boolean hasAccepted()
	{
		return accepted;
	}

	/**
	 * Resets the dialog and all of its fields.
	 */
	public void reset()
	{
		accepted = false;
		for (int i = 0; i < fields.size(); i++)
		{
			if (fields.get(i) != null)
			{
				fields.get(i).reset();
			}
		}
	}

	/**
	 * Builds the window.
	 */
	public void build()
	{
		int y = 5;
		for (int i = 0; i < fields.size(); i++)
		{
			if (fields.get(i) != null)
			{
				y = y + fields.get(i).getPane().getHeight();
				fields.get(i).getPane().setLocation(5, y - fields.get(i).getPane().getHeight());
				add(fields.get(i).getPane());
			}
		}
		h = h + y;

		setSize(w, h);
		setResizable(false);
		setLayout(null);
		setTitle(caption);
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		accept = new JButton("Ok");
		accept.setBounds((w / 2) - 125, h - 60, 120, 20);
		accept.addActionListener(this);
		add(accept);

		decline = new JButton("Abbrechen");
		decline.setBounds((w / 2) + 5, h - 60, 120, 20);
		decline.addActionListener(this);
		add(decline);

		getRootPane().setDefaultButton(accept);
		setModal(true);
		setVisible(true);
	}

	/**
	 * Checks if the user has accepted the changes in the dialog.
	 * 
	 * @param e Action event
	 */
	@Override
	public void actionPerformed(final ActionEvent e)
	{
		if (e.getSource() == accept)
		{
			accepted = true;
		}
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	/**
	 * Main method for testing.
	 * 
	 * @param args Program arguments
	 */
	public static void main(final String[] args)
	{
		final InputComponentFactory icf = new InputComponentFactoryImpl();

		final List<InputComponent> fields = new ArrayList<>();
		fields.add(icf.getBooleanBox(440, 22, "Ein sehr langes Label"));
		fields.add(icf.getBooleanBox(440, 22, "Kurzes Label"));
		fields.add(icf.getIntegerField(440, 22, "Einwortlabel"));
		fields.add(icf.getIntegerField(440, 22, "L a b e l   m i t   S p a c e"));

		final InputBehaviorArgumentsDialog id = new InputBehaviorArgumentsDialog(450, "Input", null);
		id.setFields(fields);
		id.build();
	}
}
