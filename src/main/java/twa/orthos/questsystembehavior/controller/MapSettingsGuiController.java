
package twa.orthos.questsystembehavior.controller;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import twa.orthos.questsystembehavior.gui.window.MapAssistantTab;
import twa.orthos.questsystembehavior.model.DiplomacyState;
import twa.orthos.questsystembehavior.model.MapSettings;
import twa.orthos.questsystembehavior.model.PlayerColor;
import twa.orthos.questsystembehavior.model.Weather;

/**
 * Interface controller for the map settings tab.
 * 
 * @author totalwarANGEL
 *
 */
@Getter
@Setter
public class MapSettingsGuiController
{
	private final int w;

	private final int h;

	private final MapAssistantTab tab;

	private final MapSettingsLogicController controller;

	/**
	 * Constructor
	 * 
	 * @param w Width
	 * @param h Height
	 * @param controller Logic controller
	 */
	public MapSettingsGuiController(final int w, final int h, final MapSettingsLogicController controller)
	{
		this.w = w;
		this.h = h;
		this.controller = controller;
		tab = new MapAssistantTab(w - 10, h - 115, "Kartenassistent", this);
		tab.enableTab(false);
	}

	/**
	 * Updates the settings data with the adjustments from the tab content.
	 */
	public void updateSettingsData()
	{
		// Get settings object and reset it
		final MapSettings settings = controller.getMapSettings();
		settings.reset();

		// Set weather set
		settings.setMapWeather(new Weather(tab.getSelectedWeatherSet()));

		// Set meta data
		settings.setMapMetaData(
			tab.getMapName().getText(), 
			tab.getMapAuthor().getText(), 
			tab.getMapVersion().getText()
		);

		// Set debug options
		settings.setDebugOptions(
			tab.getDebugCheck().getSelectedIndex() == 0, 
			tab.getDebugCheats().getSelectedIndex() == 0,
			tab.getDebugShell().getSelectedIndex() == 0, 
			tab.getDebugTrace().getSelectedIndex() == 0
		);

		// Set start resources
		settings.setStartResources(
			Integer.parseInt(tab.getResource(0).getText()), 
			Integer.parseInt(tab.getResource(1).getText()), 
			Integer.parseInt(tab.getResource(2).getText()), 
			Integer.parseInt(tab.getResource(3).getText()), 
			Integer.parseInt(tab.getResource(4).getText()), 
			Integer.parseInt(tab.getResource(5).getText())
		);

		// Set diplomacy sates
		for (int i = 0; i < 8; i++)
		{
			settings.addDiplomacyState(tab.getPlayerName(i + 1).getText(), i + 1, tab.getSelectedDiplomacy(i + 1));
		}

		// Set player colors
		for (int i = 0; i < 8; i++)
		{
			settings.addPlayerColor(i + 1, tab.getSelectedPlayerColor(i + 1));
		}
	}

	/**
	 * Updates the visualization of the map settings.
	 */
	public void updateSettingsDisplay()
	{
		final MapSettings settings = controller.getMapSettings();

		// Update meta info
		tab.getMapName().setText(settings.getMetaData()[0]);
		tab.getMapAuthor().setText(settings.getMetaData()[1]);
		tab.getMapVersion().setText(settings.getMetaData()[2]);

		// Update debug
		tab.getDebugCheats().setSelectedIndex((settings.getDebugOptions()[1]) ? 0 : 1);
		tab.getDebugCheck().setSelectedIndex((settings.getDebugOptions()[0]) ? 0 : 1);
		tab.getDebugShell().setSelectedIndex((settings.getDebugOptions()[2]) ? 0 : 1);
		tab.getDebugTrace().setSelectedIndex((settings.getDebugOptions()[3]) ? 0 : 1);

		// Update weather set
		tab.setSelectedWeatherSet(settings.getMapWeather().getSet());

		// Update player colors
		final List<PlayerColor> playerColors = settings.getPlayerColors();
		for (int i = 0; i < playerColors.size(); i++)
		{
			tab.setSelectedPlayerColor(playerColors.get(i).getPlayer(), playerColors.get(i).getName());
		}

		// Update diplomacy states
		final List<DiplomacyState> diplomacyStates = settings.getDiplomacyStates();
		for (int i = 0; i < diplomacyStates.size(); i++)
		{
			tab.setSelectedDiplomacy(
				diplomacyStates.get(i).getPlayer(), diplomacyStates.get(i).getName(), diplomacyStates.get(i).getDiplomacy()
			);
		}

		// Update resources
		final int[] startResources = settings.getStartResources();
		for (int i = 0; i < startResources.length; i++)
		{
			tab.getResource(i).setText("" + startResources[i]);
		}
	}
}
