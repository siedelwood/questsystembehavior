
package twa.orthos.questsystembehavior.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;

import lombok.Getter;
import lombok.Setter;
import twa.orthos.questsystembehavior.gui.dialog.BehaviorSelectionDialog;
import twa.orthos.questsystembehavior.gui.dialog.InputBehaviorArgumentsDialog;
import twa.orthos.questsystembehavior.gui.dialog.InputNameDialog;
import twa.orthos.questsystembehavior.gui.input.InputComponent;
import twa.orthos.questsystembehavior.gui.input.InputComponentFactory;
import twa.orthos.questsystembehavior.gui.input.InputComponentFactoryImpl;
import twa.orthos.questsystembehavior.gui.window.QuestAssistantTab;
import twa.orthos.questsystembehavior.model.Behavior;
import twa.orthos.questsystembehavior.model.Parameter;
import twa.orthos.questsystembehavior.model.Quest;

/**
 * Interface controller for the quest tab.
 * 
 * @author totalwarANGEL
 *
 */
@Getter
@Setter
public class QuestGuiController
{
	private QuestLogicController controller;

	private QuestAssistantTab tab;

	private final int w;

	private final int h;

	private JFrame mainWindowFrame;

	/**
	 * Constructor
	 * 
	 * @param w Width
	 * @param h Height
	 * @param controller Logic controller
	 * @param mainWindowFrame
	 */
	public QuestGuiController(
		final int w, final int h, final QuestLogicController controller, final JFrame mainWindowFrame
	)
	{
		this.w = w;
		this.h = h;
		this.controller = controller;
		this.mainWindowFrame = mainWindowFrame;
		tab = new QuestAssistantTab(w - 10, h - 115, "Missionsassistent", this);
	}

	/**
	 * Removes a behavior from the current quest.
	 */
	public void removeBehavior()
	{
		final int quest = tab.getQuestList().getSelectedIndex();
		final int idx = tab.getBehaviorList().getSelectedIndex();
		if (idx > -1)
		{
			controller.removeBehaviorFromQuest(quest, idx);
		}
		updateBehaviorList();
	}

	/**
	 * Adds a behavior to the current quest.
	 */
	public void addBehavior()
	{
		final int quest = tab.getQuestList().getSelectedIndex();
		final BehaviorSelectionDialog dialog = new BehaviorSelectionDialog(
			mainWindowFrame.getHeight() - 90, "Behavior hinzufügen", mainWindowFrame
		);
		dialog.setData(controller.getBehaviorNames());
		dialog.setDescription(controller.getBehaviorDescriptions());
		dialog.build();

		if (dialog.getSelected() != null)
		{
			final int behaviorID = controller.countBehaviorsOfQuest(quest);

			final Behavior behavior = controller.getBehaviorByName(dialog.getSelected());
			if (behavior.getFields().size() > 0)
			{
				final List<String> data = openBehaviorDetails(dialog.getSelected(), behaviorID);
				if (data != null)
				{
					final List<List<String>> behaviorList = controller.getBehaviorsOfQuest(quest);
					if (behaviorList.size() > behaviorID)
					{
						behaviorList.remove(behaviorID);
						behaviorList.add(behaviorID, data);
					}
					else
					{
						behaviorList.add(data);
					}
				}
			}
			else
			{
				final List<List<String>> behaviorList = controller.getBehaviorsOfQuest(quest);
				behaviorList.add(Arrays.asList(dialog.getSelected()));
			}
		}

		updateBehaviorList();
	}

	/**
	 * Removes a quest from the quest list if possible.
	 */
	public void removeQuest()
	{
		final int selected = tab.getQuestList().getSelectedIndex();
		if (selected > -1)
		{
			controller.getQuestsContainer().removeQuest(selected);
		}
		updateQuestList();
	}

	/**
	 * Adds a quest to the quest list if possible.
	 */
	public void addQuest()
	{
		final InputNameDialog input = new InputNameDialog(350, "Quest erstellen", mainWindowFrame);
		if (input.hasAccepted())
		{
			for (final String b : controller.getQuestNames())
			{
				if (b.equals(input.getGivenName()))
				{
					return;
				}
			}
			final String name = input.getGivenName();
			controller.getQuestsContainer().createQuest(name, "1", "0", "MAINQUEST_OPEN", "", "", false);
		}
		updateQuestList();
	}

	/**
	 * Updates the quest with the currently displayed content.
	 */
	public void updateDisplayedQuest()
	{
		final int idx = tab.getQuestList().getSelectedIndex();
		if (idx > -1)
		{
			final Quest currentQuest = controller.getQuestsContainer().getQuest(idx);
			currentQuest.setVisible(tab.getSettingsVisible().isSelected());
			currentQuest.setReceiver(tab.getSettingsPlayer().getItemAt(tab.getSettingsPlayer().getSelectedIndex()));
			currentQuest.setQuestType(tab.getSettingsType().getItemAt(tab.getSettingsType().getSelectedIndex()));
			currentQuest.setQuestTitle(tab.getSettingsTitle().getText());
			currentQuest.setTime(tab.getSettingsTime().getText());

			String text = tab.getSettingsText().getText();
			text = text.replaceAll("\n", " @cr ");
			currentQuest.setQuestText(text);

			System.out.println(currentQuest);
		}
	}

	/**
	 * Updates the details with data of the quest at the quest index.
	 * 
	 * @param selectedIndex Quest index
	 */
	private void updateDetails(final int selectedIndex)
	{
		tab.enableDetailsArea(selectedIndex > -1);
		tab.enableBehaviorArea(selectedIndex > -1);

		if (selectedIndex > -1)
		{
			final Quest selectedQuest = controller.getQuestsContainer().getQuest(selectedIndex);
			tab.getSettingsVisible().setSelected(selectedQuest.isVisible());
			tab.getSettingsPlayer().setSelectedIndex(Integer.parseInt(selectedQuest.getReceiver()) - 1);
			tab.getSettingsText().setText(selectedQuest.getQuestText());
			tab.getSettingsTitle().setText(selectedQuest.getQuestTitle());
			tab.getSettingsTime().setText(selectedQuest.getTime());
			tab.getSettingsType().setSelectedItem(selectedQuest.getQuestType());
		}
	}

	/**
	 * Updates the list of quests.
	 */
	public void updateQuestList()
	{
		controller.sortQuestBehaviors();
		tab.getQuestList().setListData(controller.getQuestNames());
	}

	/**
	 * Updates the list of behaviors of the current quest.
	 */
	private void updateBehaviorList()
	{
		final int quest = tab.getQuestList().getSelectedIndex();
		controller.sortBehaviorsOfQuest(quest);
		tab.getBehaviorList().setListData(controller.getNamesOfBehaviorOfQuest(quest));
	}

	/**
	 * Opens the behavior properties for the selected behavior.
	 * 
	 * @param selectedIndex Behavior index
	 */
	public void onBehaviorDoubleClicked(final int selectedIndex)
	{
		final int quest = tab.getQuestList().getSelectedIndex();
		final List<String> selectedName = controller.getQuestsContainer().getQuest(quest)
			.getSingleBehavior(selectedIndex);

		final Behavior behavior = controller.getBehaviorByName(selectedName.get(0));
		System.out.println(behavior);
		if (behavior.getFields().size() > 0)
		{
			final List<String> data = openBehaviorDetails(selectedName.get(0), selectedIndex);
			if (data != null)
			{
				final List<List<String>> behaviorList = controller.getBehaviorsOfQuest(quest);
				if (behaviorList.size() > selectedIndex)
				{
					behaviorList.remove(selectedIndex);
					behaviorList.add(selectedIndex, data);
				}
				else
				{
					behaviorList.add(data);
				}
			}
		}
		updateBehaviorList();
	}

	/**
	 * Opens the properties of the behavior.
	 * 
	 * @param behavior Behavior name
	 * @param selectedIndex Behavior index
	 * @return Arguments as list
	 */
	private List<String> openBehaviorDetails(final String behavior, final int selectedIndex)
	{
		final int quest = tab.getQuestList().getSelectedIndex();
		final InputComponentFactory factory = new InputComponentFactoryImpl();

		final List<Parameter> parameters = controller.getBehaviorParameter(behavior);
		final List<InputComponent> fields = new ArrayList<>();
		for (int i = 0; i < parameters.size(); i++)
		{
			int h = 22;
			if (parameters.get(i).getType().equals("MessageInput"))
			{
				h = 60;
			}
			fields.add(factory.getFromParameterData((mainWindowFrame.getWidth() / 2) - 8, h, parameters.get(i)));
		}

		if (controller.countBehaviorsOfQuest(quest) > selectedIndex)
		{
			final List<String> oldData = controller.getSingleBehaviorOfQuest(quest, selectedIndex);
			System.out.println(oldData);
			for (int j = 1; j < oldData.size(); j++)
			{
				if (fields.get(j - 1) != null)
				{
					fields.get(j - 1).setValue(oldData.get(j));
				}
			}
		}

		final InputBehaviorArgumentsDialog dialog = new InputBehaviorArgumentsDialog(
			mainWindowFrame.getWidth() / 2, behavior, mainWindowFrame
		);
		dialog.setFields(fields);
		dialog.build();
		return dialog.toList();
	}

	/**
	 * Callback when a quest is selected.
	 * 
	 * @param selectedIndex Quest index
	 */
	public void onQuestSelected(final int selectedIndex)
	{
		updateDetails(selectedIndex);
		updateBehaviorList();
	}

	/**
	 * Callback when a behavior is selected.
	 * 
	 * @param selectedIndex Behavior index
	 */
	public void onBehaviorSelected(final int selectedIndex)
	{
	}
}
