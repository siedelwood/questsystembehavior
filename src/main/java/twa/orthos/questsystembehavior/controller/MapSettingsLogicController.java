
package twa.orthos.questsystembehavior.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import twa.orthos.questsystembehavior.model.MapSettings;
import twa.orthos.questsystembehavior.service.save.settings.SettingsReader;
import twa.orthos.questsystembehavior.service.save.settings.SettingsSerializer;

/**
 * Controlls the pure logic of the map settings tab.
 * 
 * @author totalwarANGEL
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MapSettingsLogicController
{
	protected MapSettings mapSettings;

	protected SettingsReader settingsReader;

	protected SettingsSerializer settingsSerializer;

	/**
	 * Loads the data from the configuration file.
	 * 
	 * @param filename file Name
	 * @throws Exception
	 */
	public void loadMissionData(final String filename) throws Exception
	{
		settingsReader.load(filename);
		mapSettings = settingsReader.parseSettings();
	}

	/**
	 * Returns a string with the serialized settings.
	 * 
	 * @return Serialized briefings
	 */
	public String serializeSettings()
	{
		return settingsSerializer.serializeSettings(mapSettings);
	}
}
