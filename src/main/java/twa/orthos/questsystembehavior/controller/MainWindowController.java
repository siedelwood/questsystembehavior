
package twa.orthos.questsystembehavior.controller;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import twa.orthos.questsystembehavior.gui.window.MainWindow;
import twa.orthos.questsystembehavior.model.FileChooserResult;
import twa.orthos.questsystembehavior.service.FileChooserService;
import twa.orthos.questsystembehavior.service.lua.BriefingBuilder;
import twa.orthos.questsystembehavior.service.lua.LuaBriefingBuilder;
import twa.orthos.questsystembehavior.service.lua.LuaQuestBuilder;
import twa.orthos.questsystembehavior.service.lua.MapScriptBuilder;
import twa.orthos.questsystembehavior.service.lua.QuestBuilder;
import twa.orthos.questsystembehavior.service.map.MapLoader;
import twa.orthos.questsystembehaviormanual.controller.ManualGuiController;
import twa.orthos.questsystembehaviormanual.controller.ManualLogicController;

/**
 * Controller for the main window.
 * 
 * @author totalwarANGEL
 */
public class MainWindowController implements WindowListener
{

    protected final MainWindowController self;

    protected MainWindow mainWindow;

    protected JFrame mainWindowFrame;

    protected QuestLogicController questController;

    protected QuestGuiController questGuiController;

    protected BriefingLogicController briefingController;

    protected BriefingGuiController briefingGuiController;

    protected MapSettingsLogicController settingsController;

    protected MapSettingsGuiController settingsGuiController;

    protected ManualLogicController manualController;

    protected ManualGuiController manualGuiController;

    protected FileChooserService fileChooser;

    protected MapLoader mapLoader;

    private String currentMapPath;

    private int w;

    private int h;

    /**
     * Sets the controller for the window.
     * 
     * @throws InterfaceControllerException
     */
    public MainWindowController() throws InterfaceControllerException
    {
        self = this;
    }

    /**
     * Sets the file chooser service.
     * 
     * @param FileChooserService File chooser
     */
    public void setFileChooser(final FileChooserService fileChooser)
    {
        this.fileChooser = fileChooser;
    }

    /**
     * Sets the settings controller.
     * 
     * @param settingsController Settings controller
     */
    public void setMapController(final MapSettingsLogicController settingsController)
    {
        this.settingsController = settingsController;
    }

    /**
     * Sets the quest CRUD controller.
     * 
     * @param questController Quest controller
     */
    public void setQuestController(final QuestLogicController questController)
    {
        this.questController = questController;
    }

    /**
     * Sets the briefing CRUD controller.
     * 
     * @param briefingController Briefing controller
     */
    public void setBriefingController(final BriefingLogicController briefingController)
    {
        this.briefingController = briefingController;
    }

    public void setManualController(final ManualLogicController manualController)
    {
        this.manualController = manualController;
    }

    /**
     * Sets the map loader service.
     * 
     * @param mapLoader Map loader
     */
    public void setMapLoader(final MapLoader mapLoader)
    {
        this.mapLoader = mapLoader;
    }

    /**
     * Runs the GUI in another thread than the logic.
     */
    public void run(final int x, final int y)
    {
        SwingUtilities.invokeLater(() -> {
            mainWindow = new MainWindow(x, y, self);
            mainWindow.buildWindow();
            mainWindow.setVisible(true);

            mainWindowFrame = new JFrame();
            mainWindowFrame.setTitle("Siedler 5 Skriptassistent");
            mainWindowFrame.setSize(x, y);
            mainWindowFrame.setLocationRelativeTo(null);
            mainWindowFrame.setResizable(false);
            mainWindowFrame.add(mainWindow);
            mainWindowFrame.addWindowListener(this);
            mainWindowFrame.setVisible(true);

            w = x;
            h = y;
            onFrameCreated();
        });
    }

    /**
     * Creates the window contents after the frame is ready.
     */
    private void onFrameCreated()
    {
        settingsGuiController = new MapSettingsGuiController(w, h, settingsController);
        briefingGuiController = new BriefingGuiController(w, h, briefingController, mainWindowFrame);
        questGuiController = new QuestGuiController(w, h, questController, mainWindowFrame);

        mainWindow.getTabPane().addTab(settingsGuiController.getTab().getTabCaption(), settingsGuiController.getTab());
        mainWindow.getTabPane().addTab(questGuiController.getTab().getTabCaption(), questGuiController.getTab());
        mainWindow.getTabPane().addTab(briefingGuiController.getTab().getTabCaption(), briefingGuiController.getTab());

        final int x = mainWindow.getTabPane().getWidth();
        final int y = mainWindow.getTabPane().getHeight();
        manualGuiController = new ManualGuiController(x, y, manualController, mainWindowFrame);
        mainWindow.getTabPane().addTab(manualGuiController.getTab().getTabCaption(), manualGuiController.getTab());
    }

    /**
     * Saves the current mission to a configuration file.
     * 
     * @throws InterfaceControllerException
     */
    private void saveData() throws InterfaceControllerException
    {
        settingsGuiController.updateSettingsData();
        try
        {
            final FileChooserResult result = fileChooser.openSaveDataChooser();
            if (result.isApproved())
            {
                final File file = result.getSelectedFile();
                final String quests = questController.serializeQuests();
                final String settings = settingsController.serializeSettings();
                final String briefings = briefingController.serializeBriefings();
                final String json = String.format("[{%s,%s,%s}]", settings, quests, briefings);

                final OutputStreamWriter writer = new OutputStreamWriter(
                    new FileOutputStream(file), StandardCharsets.UTF_8
                );
                writer.write(json);
                writer.flush();
                writer.close();
            }
        } catch (final Exception e)
        {
            throw new InterfaceControllerException(e);
        }
    }

    /**
     * Loads settings from a configuration file into the current session.
     * 
     * @throws InterfaceControllerException
     */
    public void loadData() throws InterfaceControllerException
    {
        try
        {
            final FileChooserResult result = fileChooser.openLoadDataChooser();
            if (result.isApproved())
            {
                final File file = result.getSelectedFile();

                questController.loadMissionData(file.getAbsolutePath());
                questController.getBehaviorTypes();
                questGuiController.getTab().getQuestList().setSelectedIndex(-1);
                questGuiController.updateQuestList();

                briefingController.loadMissionData(file.getAbsolutePath());
                briefingGuiController.getTab().getBriefingTree().setSelectionRow(0);
                briefingGuiController.updateBriefingTree();

                settingsController.loadMissionData(file.getAbsolutePath());
                settingsGuiController.updateSettingsDisplay();
            }
        } catch (final Exception e)
        {
            throw new InterfaceControllerException(e);
        }
    }

    /**
     * Saves the map with the current settings and cleans up interface and mess.
     * 
     * @throws InterfaceControllerException
     */
    private void saveMap() throws InterfaceControllerException
    {
        settingsGuiController.updateSettingsData();
        try
        {
            // Copy library files
            final String sourceDir = "lua/orthus/lua/qsb";
            final String destDir = "/maps/externalmap/qsb";
            mapLoader.selectMap(currentMapPath + ".unpacked");
            mapLoader.add(sourceDir + "/oop.lua", destDir + "/oop.lua");
            mapLoader.add(sourceDir + "/questsystem.lua", destDir + "/questsystem.lua");
            mapLoader.add(sourceDir + "/information.lua", destDir + "/information.lua");
            mapLoader.add(sourceDir + "/information_ex2.lua", destDir + "/information_ex2.lua");
            mapLoader.add(sourceDir + "/information_ex3.lua", destDir + "/information_ex3.lua");
            mapLoader.add(sourceDir + "/interaction.lua", destDir + "/interaction.lua");
            mapLoader.add(sourceDir + "/questbehavior.lua", destDir + "/questbehavior.lua");
            mapLoader.add(sourceDir + "/questdebug.lua", destDir + "/questdebug.lua");

            // Create quest builder
            final QuestBuilder questBuilder = new LuaQuestBuilder();
            questBuilder.setBehaviorTypes(questController.getBehaviorTypes());
            questBuilder.setQuestList(questController.getQuestsContainer().getQuestList());
            final BriefingBuilder briefingBuilder = new LuaBriefingBuilder();
            briefingBuilder.setBriefings(briefingController.getBriefingContainer().getBriefingList());

            // Create script builder
            final MapScriptBuilder mapScriptBuilder = new MapScriptBuilder();
            mapScriptBuilder.setQuestBuilder(questBuilder);
            mapScriptBuilder.setBriefingBuilder(briefingBuilder);
            mapScriptBuilder.setMapSettings(settingsController.getMapSettings());

            // Replace tokens in intern mapscript
            final ByteArrayInputStream out = new ByteArrayInputStream(
                mapScriptBuilder.replaceTokensInMapscript("lua/mainmapscript.lua").getBytes()
            );
            mapLoader.add(out, "/maps/externalmap/mainmapscript.lua");

            // Copy main script (or default)
            final String mainScriptPath = currentMapPath.substring(0, currentMapPath.lastIndexOf(File.separator));
            if (!Files.exists(Paths.get(mainScriptPath + "/mapscript.lua")))
            {
                Files.copy(Paths.get("lua/mapscript.lua"), Paths.get(mainScriptPath + "/mapscript.lua"));
            }
            mapLoader.add(mainScriptPath + "/mapscript.lua", "/maps/externalmap/mapscript.lua");

            // Pack the map
            mapLoader.packMap();
            deleteDirectory(new File(currentMapPath + ".unpacked"));
            settingsGuiController.getTab().enableTab(false);
            settingsGuiController.getTab().clean();
            briefingGuiController.getTab().enablePropertiesView(false);
            briefingGuiController.getTab().enableTreeView(false);
            briefingGuiController.getTab().clean();
            questGuiController.getTab().enableDetailsArea(false);
            questGuiController.getTab().enableBehaviorArea(false);
            questGuiController.getTab().enableQuestArea(false);
            questGuiController.getTab().clean();
            mainWindow.getSaveMapButton().setEnabled(false);
            mainWindow.getOpenButton().setEnabled(false);
            mainWindow.getSaveButton().setEnabled(false);
        } catch (final Exception e)
        {
            throw new InterfaceControllerException(e);
        }
    }

    /**
     * Deleates a directory.
     * 
     * @param  directory
     * @return
     */
    public static boolean deleteDirectory(final File directory)
    {
        if (directory.exists())
        {
            final File[] files = directory.listFiles();
            if (null != files)
            {
                for (final File file : files)
                {
                    if (file.isDirectory())
                    {
                        deleteDirectory(file);
                    } else
                    {
                        file.delete();
                    }
                }
            }
        }
        return (directory.delete());
    }

    /**
     * Uncompresses a map.
     * 
     * @throws InterfaceControllerException
     */
    private void openMap() throws InterfaceControllerException
    {
        try
        {
            final FileChooserResult result = fileChooser.openLoadMapChooser();
            if (result.isApproved())
            {
                final File file = result.getSelectedFile();
                currentMapPath = file.getAbsolutePath();
                mapLoader.selectMap(currentMapPath);
                mapLoader.unpackMap();

                final String mainScriptPath = currentMapPath.substring(0, currentMapPath.lastIndexOf(File.separator));
                if (!Files.exists(Paths.get(mainScriptPath + "/mapscript.lua")))
                {
                    Files.copy(Paths.get("lua/mapscript.lua"), Paths.get(mainScriptPath + "/mapscript.lua"));
                }

                mainWindow.getSaveMapButton().setEnabled(true);
                mainWindow.getSaveButton().setEnabled(true);
                mainWindow.getOpenButton().setEnabled(true);
                mainWindow.getMapPathField().setText(file.getAbsolutePath());

                questGuiController.getTab().enableQuestArea(true);
                briefingGuiController.getTab().enablePropertiesView(true);
                briefingGuiController.getTab().enableTreeView(true);

                settingsGuiController.getTab().enableTab(true);
            }
        } catch (final Exception e)
        {
            throw new InterfaceControllerException(e);
        }
    }

    /**
     * Saves the map and conceals possible exceptions.
     */
    public void requestSaveMap()
    {
        try
        {
            saveMap();
        } catch (final InterfaceControllerException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Loads the map and conceals possible exceptions.
     */
    public void requestLoadMap()
    {
        try
        {
            openMap();
        } catch (final InterfaceControllerException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Saves the configuration and conceals possible exceptions.
     */
    public void requestCreateSave()
    {
        try
        {
            saveData();
        } catch (final InterfaceControllerException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Loads the configuration and conceals possible exceptions.
     */
    public void requestLoadSave()
    {
        try
        {
            loadData();
        } catch (final InterfaceControllerException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Window activated event.
     */
    @Override
    public void windowActivated(final WindowEvent arg0)
    {
    }

    /**
     * Window closed event.
     */
    @Override
    public void windowClosed(final WindowEvent e)
    {
    }

    /**
     * Window closing event.
     */
    @Override
    public void windowClosing(final WindowEvent e)
    {
        if (currentMapPath != null)
        {
            deleteDirectory(new File(currentMapPath + ".unpacked"));
        }
        System.exit(0);
    }

    /**
     * Window deactivated event.
     */
    @Override
    public void windowDeactivated(final WindowEvent e)
    {
    }

    /**
     * Window deiconified event.
     */
    @Override
    public void windowDeiconified(final WindowEvent e)
    {
    }

    /**
     * Window iconified event.
     */
    @Override
    public void windowIconified(final WindowEvent e)
    {
    }

    /**
     * Window opened event.
     */
    @Override
    public void windowOpened(final WindowEvent e)
    {
    }
}
