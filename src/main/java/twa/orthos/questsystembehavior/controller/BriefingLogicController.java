
package twa.orthos.questsystembehavior.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import twa.orthos.questsystembehavior.container.briefing.BriefingsContainer;
import twa.orthos.questsystembehavior.service.save.briefing.BriefingReader;
import twa.orthos.questsystembehavior.service.save.briefing.BriefingSerializer;

/**
 * Controller for the pure logic of the briefing tab.
 * 
 * @author totalwarANGEL
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BriefingLogicController
{
	protected BriefingsContainer briefingContainer;

	protected BriefingReader briefingReader;

	protected BriefingSerializer briefingSerializer;

	/**
	 * Loads the data from the configuration file.
	 * 
	 * @param filename file Name
	 * @throws Exception
	 */
	public void loadMissionData(final String filename) throws Exception
	{
		briefingReader.load(filename);
		briefingContainer.setBriefingList(briefingReader.parseBriefings());
	}

	/**
	 * Returns a string with the serialized Briefings.
	 * 
	 * @return Serialized briefings
	 */
	public String serializeBriefings()
	{
		return briefingSerializer.serializeBriefings(briefingContainer.getBriefingList());
	}

	/**
	 * Sorts the briefings.
	 */
	public void sortBriefings()
	{
		briefingContainer.sort();
	}
}
