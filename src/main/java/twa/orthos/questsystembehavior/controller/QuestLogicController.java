
package twa.orthos.questsystembehavior.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import twa.orthos.questsystembehavior.container.quest.BehaviorTypeContainer;
import twa.orthos.questsystembehavior.container.quest.QuestsContainer;
import twa.orthos.questsystembehavior.model.Behavior;
import twa.orthos.questsystembehavior.model.Parameter;
import twa.orthos.questsystembehavior.model.Quest;
import twa.orthos.questsystembehavior.service.save.behavior.BehaviorReader;
import twa.orthos.questsystembehavior.service.save.quest.QuestReader;
import twa.orthos.questsystembehavior.service.save.quest.QuestSerializer;

/**
 * Controlls the pure logic of the quest tab.
 * @author totalwarANGEL
 *
 */
@AllArgsConstructor
@Getter
@Setter
public class QuestLogicController
{
	protected QuestReader questReader;

	protected BehaviorReader behaviorReader;

	protected QuestSerializer questSerializer;

	protected QuestsContainer questsContainer;

	protected BehaviorTypeContainer behaviorTypeContainer;

	public void loadMissionData(final String filename) throws Exception
	{
		questReader.load(filename);
		questsContainer.setQuestList(questReader.parseQuests());
	}

	/**
	 * Parses the behavior types from the configuration file.
	 * 
	 * @return List of behavior templates
	 */
	public List<Behavior> getBehaviorTypes()
	{
		final List<Behavior> behaviors = behaviorReader.parseBehavior();
		behaviorTypeContainer.setBehaviors(behaviors);
		return behaviorTypeContainer.getBehaviors();
	}

	/**
	 * Returns a vector with the names of the known behavior templates.
	 * 
	 * @return Behavior name list
	 */
	public Vector<String> getBehaviorNames()
	{
		final List<Behavior> behaviors = getBehaviorTypes();
		final Vector<String> names = new Vector<>();
		for (int i = 0; i < behaviors.size(); i++)
		{
			names.add(behaviors.get(i).getName());
		}
		return names;
	}

	/**
	 * Returns the descriptions of all behavior templates as vector of strings.
	 * 
	 * @return Descriptions Description texts
	 */
	public Vector<String> getBehaviorDescriptions()
	{
		final List<Behavior> behaviors = getBehaviorTypes();
		final Vector<String> desciptions = new Vector<>();
		for (int i = 0; i < behaviors.size(); i++)
		{
			desciptions.add(behaviors.get(i).getDescription());
		}
		return desciptions;
	}

	/**
	 * Returns the behavior template with the desired name.
	 * 
	 * @param name Behavior name
	 * @return Behavior template
	 */
	public Behavior getBehaviorByName(final String name)
	{
		return behaviorTypeContainer.getBehaviorByName(name);
	}

	/**
	 * Returns the behavior at the index of the quest.
	 * 
	 * @param quest Quest index
	 * @param idx behavior index
	 * @return Flattened behavior
	 */
	public List<String> getBehaviorOfQuestByIndex(final int quest, final int idx)
	{
		return questsContainer.getQuest(quest).getSingleBehavior(idx);
	}

	/**
	 * Returns the parameter of the behavior.
	 * 
	 * @param name Behavior name
	 * @return Parameter list
	 */
	public List<Parameter> getBehaviorParameter(final String name)
	{
		return behaviorTypeContainer.getBehaviorParameter(name);
	}

	/**
	 * Returns a vector of behaviors attached to the quest.
	 * 
	 * @param quest Quest index
	 * @return Behavior name list
	 */
	public Vector<String> getNamesOfBehaviorOfQuest(final int quest)
	{
		final Vector<String> names = new Vector<>();
		if (questsContainer.getQuestList().size() > quest && questsContainer.getQuest(quest) != null)
		{
			final List<List<String>> behaviors = questsContainer.getQuest(quest).getBehavior();
			for (int i = 0; i < behaviors.size(); i++)
			{
				names.add(behaviors.get(i).get(0));
			}
		}
		return names;
	}

	/**
	 * Sorts the quests.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void sortQuestBehaviors()
	{
		final Comparator c = (a, b) -> ((Quest) a).getName().compareTo(((Quest) b).getName());
		questsContainer.getQuestList().sort(c);
	}

	/**
	 * Returns a vector of quest names for the quest list in the graphical
	 * interface.
	 * 
	 * @return Quest name list
	 */
	public Vector<String> getQuestNames()
	{
		return questsContainer.getQuestNames();
	}

	/**
	 * Adds a behavior to a quest at the end of it's behavior list and returns the
	 * current last index.
	 * 
	 * @param quest Quest index
	 * @param selected Behavior name
	 * @return Last index
	 */
	public int addBehaviorToQuest(final int quest, final String selected)
	{
		final Behavior behavior = behaviorTypeContainer.getBehaviorByName(selected);
		final List<String> flatBehavior = new ArrayList<>();
		flatBehavior.add(selected);
		for (int i = 0; i < behavior.getFields().size(); i++)
		{
			flatBehavior.add("");
		}

		questsContainer.getQuest(quest).getBehavior().add(flatBehavior);
		return questsContainer.getQuest(quest).getBehavior().size() - 1;
	}

	/**
	 * Removes a behavior from a quest.
	 * 
	 * @param quest Quest index
	 * @param idx Behavior index
	 */
	public void removeBehaviorFromQuest(final int quest, final int idx)
	{
		if (getQuestsContainer().getQuestList().size() <= quest)
		{
			return;
		}
		if (questsContainer.getQuest(quest).getBehavior().size() <= idx)
		{
			return;
		}
		questsContainer.getQuest(quest).getBehavior().remove(idx);
	}

	/**
	 * Returns the number of behaviors of a quest.
	 * 
	 * @param quest Quest to check
	 * @return Amount of behavior
	 */
	public int countBehaviorsOfQuest(final int quest)
	{
		if (getQuestsContainer().getQuestList().size() <= quest)
		{
			return 0;
		}
		return getQuestsContainer().getQuest(quest).getBehavior().size();
	}

	/**
	 * Sorts the behavior of an quest.
	 * 
	 * @param index Quest index
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void sortBehaviorsOfQuest(final int index)
	{
		final Comparator c = (a, b) -> ((List<String>) a).get(0).compareTo(((List<String>) b).get(0));
		final Quest quest = getQuestsContainer().getQuest(index);
		if (quest != null)
		{
			quest.getBehavior().sort(c);
		}
	}

	/**
	 * Returns all quest behavior of the quest
	 * 
	 * @param quest Quest index
	 * @return Behavior data
	 */
	public List<List<String>> getBehaviorsOfQuest(final int quest)
	{
		if (getQuestsContainer().getQuestList().size() <= quest)
		{
			return null;
		}
		return getQuestsContainer().getQuest(quest).getBehavior();
	}

	/**
	 * Returns the quest behavior at the index of the quest.
	 * 
	 * @param quest Quest index
	 * @param idx Behavior index
	 * @return Behavior data
	 */
	public List<String> getSingleBehaviorOfQuest(final int quest, final int idx)
	{
		if (getQuestsContainer().getQuestList().size() <= quest)
		{
			return null;
		}
		if (questsContainer.getQuest(quest).getBehavior().size() <= idx)
		{
			return null;
		}
		return getQuestsContainer().getQuest(quest).getBehavior().get(idx);
	}

	public String serializeQuests()
	{
		return questSerializer.serializeQuests(questsContainer.getQuestList());
	}
}
