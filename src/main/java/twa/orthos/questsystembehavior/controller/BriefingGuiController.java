
package twa.orthos.questsystembehavior.controller;

import java.util.List;
import javax.swing.JFrame;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import lombok.Getter;
import lombok.Setter;
import twa.orthos.questsystembehavior.gui.dialog.InputNameDialog;
import twa.orthos.questsystembehavior.gui.tree.TreeUserDataPage;
import twa.orthos.questsystembehavior.gui.tree.TreeUserDataRoot;
import twa.orthos.questsystembehavior.gui.window.BriefingAssistantTab;
import twa.orthos.questsystembehavior.model.Briefing;
import twa.orthos.questsystembehavior.model.BriefingModel;
import twa.orthos.questsystembehavior.model.Page;
import twa.orthos.questsystembehavior.model.PageModel;
import twa.orthos.questsystembehavior.service.mapper.MutableTreeNodeMapper;

/**
 * Interface controller for the briefing assistent tab.
 * 
 * @author totalwarANGEL
 */
@Getter
@Setter
public class BriefingGuiController
{

    private final int w;

    private final int h;

    private final BriefingAssistantTab tab;

    private final BriefingLogicController controller;

    private final JFrame mainWindowFrame;

    /**
     * Constructor
     * 
     * @param w               Width
     * @param h               Height
     * @param controller      Logic controller
     * @param mainWindowFrame Main window
     */
    public BriefingGuiController(
        final int w, final int h, final BriefingLogicController controller, final JFrame mainWindowFrame
    )
    {
        this.w = w;
        this.h = h;
        this.controller = controller;
        this.mainWindowFrame = mainWindowFrame;
        tab = new BriefingAssistantTab(w - 10, h - 115, "Briefingassistent", this);

        tab.enablePropertiesView(false);
        tab.enableTreeView(false);
    }

    /**
     * Displays the properties of the tree root.
     */
    public void displayTreeRoot()
    {
        tab.getRootBox().setVisible(true);
        tab.getBranchBox().setVisible(false);
        tab.getLeaveBox().setVisible(false);
    }

    /**
     * Displays the properties of a briefing.
     */
    public void displayTreeBranch()
    {
        tab.getRootBox().setVisible(false);
        tab.getBranchBox().setVisible(true);
        tab.getLeaveBox().setVisible(false);
    }

    /**
     * Displays the properties of a briefing page.
     * 
     * @param selectedNode Page data
     */
    public void displayTreeLeave(final DefaultMutableTreeNode selectedNode)
    {
        tab.getRootBox().setVisible(false);
        tab.getBranchBox().setVisible(false);
        tab.getLeaveBox().setVisible(true);

        final TreeUserDataPage page = (TreeUserDataPage) selectedNode.getUserObject();
        tab.getPageTitle().setText(page.getPageTitle());
        tab.getPageText().setText(page.getPageText());
        tab.getPageAction().setText(page.getPageAction());
        tab.getPagePosition().setText(page.getPagePosition());
        tab.getPageDialog().setSelectedIndex((page.isDialogPage()) ? 0 : 1);
        tab.getDisplaySky().setSelectedIndex((page.isShowSky()) ? 0 : 1);
        tab.getHideFogOfWar().setSelectedIndex((page.isHideFog()) ? 0 : 1);
    }

    /**
     * Adds a briefing page to the tree.
     * 
     * @param selectedNode
     */
    public void addBriefingPage(final DefaultMutableTreeNode selectedNode)
    {
        final int index = selectedNode.getParent().getIndex(selectedNode);
        final Briefing briefing = controller.getBriefingContainer().getBriefing(index);
        briefing.addPage(new PageModel("", "", "", "", true, false, false));
        updateBriefingTree();
        System.out.println(briefing.getPages().size());
        System.out.println(controller.getBriefingContainer().getBriefing(index).getPages().size());

        tab.getBriefingTree().setSelectionRow(index + 1);
        tab.getBriefingTree().expandRow(index + 1);
    }

    /**
     * Removes a briefing page from the tree.
     * 
     * @param selectedNode
     */
    public void removeBriefingPage(final DefaultMutableTreeNode selectedNode)
    {
        final int pageIndex = selectedNode.getParent().getIndex(selectedNode);
        final int briefingIndex = tab.getBriefingTopNode().getIndex(selectedNode.getParent());

        controller.getBriefingContainer().getBriefing(briefingIndex).removePage(pageIndex);
        updateBriefingTree();
        tab.getBriefingTree().setSelectionRow(briefingIndex + 1);
        tab.getBriefingTree().expandRow(briefingIndex + 1);
    }

    /**
     * Save the current content of the page properties to logic.
     * 
     * @param page Current page
     */
    public void saveCurrentBriefingPage(final DefaultMutableTreeNode page)
    {
        final int briefingIndex = page.getParent().getParent().getIndex(page.getParent());
        final Briefing briefing = controller.getBriefingContainer().getBriefing(briefingIndex);
        final DefaultMutableTreeNode parent = (DefaultMutableTreeNode) page.getParent();
        final int index = parent.getIndex(page);
        
        // Update page model
        final Page pageModel = briefing.getPage(index);
        pageModel.setPosition(tab.getPagePosition().getText());
        pageModel.setTitle(tab.getPageTitle().getText());
        pageModel.setText(tab.getPageText().getText());
        pageModel.setAction(tab.getPageAction().getText());
        pageModel.setDialog(tab.getPageDialog().getSelectedIndex() == 0);
        pageModel.setSkyShown(tab.getDisplaySky().getSelectedIndex() == 0);
        pageModel.setFogDisabled(tab.getHideFogOfWar().getSelectedIndex() == 0);
        
        // update page data
        final TreeUserDataPage pageData = (TreeUserDataPage) page.getUserObject();
        pageData.setPagePosition(tab.getPagePosition().getText());
        pageData.setPageTitle(tab.getPageTitle().getText());
        pageData.setPageText(tab.getPageText().getText());
        pageData.setPageAction(tab.getPageAction().getText());
        pageData.setDialogPage(tab.getPageDialog().getSelectedIndex() == 0);
        pageData.setSkyShown(tab.getDisplaySky().getSelectedIndex() == 0);
        pageData.setFogDisabled(tab.getHideFogOfWar().getSelectedIndex() == 0);
    }

    /**
     * Adds an briefing to the tree.
     */
    public void addBriefing()
    {
        final InputNameDialog name = new InputNameDialog((int) (w * 0.5), "Namen bestimmen", null);
        if (name.hasAccepted())
        {
            for (final Briefing b : controller.getBriefingContainer().getBriefingList())
            {
                if (b.getName().equals(name.getGivenName()))
                {
                    return;
                }
            }
            final Briefing briefing = new BriefingModel(name.getGivenName());
            controller.getBriefingContainer().addBriefing(briefing);
            controller.sortBriefings();
        }
        updateBriefingTree();
    }

    /**
     * Removes the selected briefing from the tree.
     * 
     * @param selectedNode Selected briefing
     */
    public void removeBriefing(final DefaultMutableTreeNode selectedNode)
    {
        final int index = selectedNode.getParent().getIndex(selectedNode);
        controller.getBriefingContainer().removeBriefing(index);
        controller.sortBriefings();
        updateBriefingTree();
        tab.getBriefingTree().setSelectionRow(0);
    }

    /**
     * Updates the quest list after changes.
     */
    public void updateBriefingTree()
    {
        controller.sortBriefings();
        
        final DefaultMutableTreeNode top = new DefaultMutableTreeNode(new TreeUserDataRoot("Briefings"));
        final List<Briefing> briefings = controller.getBriefingContainer().getBriefingList();
        final MutableTreeNodeMapper briefingMapper = new MutableTreeNodeMapper();

        for (int i = 0; i < briefings.size(); i++)
        {
            top.add(briefingMapper.toBriefingTreeNode(briefings.get(i)));
        }
        tab.setBriefingTopNode(top);
        final DefaultTreeModel model = (DefaultTreeModel) tab.getBriefingTree().getModel();
        model.setRoot(top);
    }
}
