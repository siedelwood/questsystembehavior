package twa.orthos.questsystembehavior.model;

import org.json.simple.JSONArray;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class DiplomacyState
{
	private String name;
	
	private int player;
	
	private String diplomacy;
	
	public String toJson()
	{
		return "[\"" + name + "\", " + player + ", \"" + diplomacy + "\"]";
	}
	
	public static DiplomacyState toObject(final JSONArray data)
	{
		final DiplomacyState object = new DiplomacyState();
		object.setName((String) data.get(0));
		object.setPlayer((int) data.get(1));
		object.setDiplomacy((String) data.get(2));
		return object;
	}
}
