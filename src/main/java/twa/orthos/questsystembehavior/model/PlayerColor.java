package twa.orthos.questsystembehavior.model;

import org.json.simple.JSONArray;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlayerColor
{
	private int player;
	
	private String name;
	
	public String toJson()
	{
		return "[" + player + ", \"" + name + "\"]";
	}
	
	public static PlayerColor toObject(final JSONArray data)
	{
		final PlayerColor object = new PlayerColor();
		object.setName((String) data.get(1));
		object.setPlayer((int) data.get(0));
		return object;
	}
}
