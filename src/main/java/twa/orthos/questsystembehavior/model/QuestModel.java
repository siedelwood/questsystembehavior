
package twa.orthos.questsystembehavior.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QuestModel implements Quest
{
	protected String name;

	protected String time;

	protected String receiver;

	protected String questTitle;

	protected String questText;

	protected String questType;

	protected List<List<String>> behavior;

	protected boolean visible;

	public QuestModel()
	{
		behavior = new ArrayList<List<String>>();
	}

	public QuestModel(
		final String name, final String time, final String receiver, final String questTitle, final String questText,
		final String questType
	)
	{
		this.name = name;
		this.time = time;
		this.receiver = receiver;
		this.questTitle = questTitle;
		this.questText = questText;
		this.questType = questType;

		behavior = new ArrayList<>();
	}

	public void addBehavior(final List<String> element)
	{
		behavior.add(element);
	}

	@Override
	public List<List<String>> getBehavior()
	{
		return behavior;
	}

	@Override
	public List<String> getSingleBehavior(final int index)
	{
		if (behavior.size() - 1 < index)
		{
			return null;
		}
		return behavior.get(index);
	}

	@Override
	public int countBehavior()
	{
		return behavior.size();
	}
}
