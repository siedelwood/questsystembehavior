
package twa.orthos.questsystembehavior.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PageModel implements Page
{
	protected String title;

	protected String text;

	protected String position;

	protected String action;

	protected boolean dialog;

	protected boolean skyShown;

	protected boolean fogDisabled;
}
