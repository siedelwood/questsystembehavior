
package twa.orthos.questsystembehavior.model;

import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Model of the map settings.
 * 
 * @author totalwarANGEL
 */
public class MapSettingsModel implements MapSettings
{
	private String[] metaData;

	private boolean[] debugOptions;

	private int[] startResources;

	private Weather mapWeather;

	private List<DiplomacyState> diplomacyStates;

	private List<PlayerColor> playerColors;

	/**
	 * Constructor
	 */
	public MapSettingsModel()
	{
		diplomacyStates = new ArrayList<>();
		playerColors = new ArrayList<>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMapMetaData(final String name, final String author, final String version)
	{
		metaData = new String[3];
		metaData[0] = name;
		metaData[1] = author;
		metaData[2] = version;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDebugOptions(final boolean check, final boolean cheats, final boolean shell, final boolean trace)
	{
		debugOptions = new boolean[4];
		debugOptions[0] = check;
		debugOptions[1] = cheats;
		debugOptions[2] = shell;
		debugOptions[3] = trace;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStartResources(
		final int gold, final int clay, final int wood, final int stone, final int iron, final int sulfur
	)
	{
		startResources = new int[6];
		startResources[0] = gold;
		startResources[1] = clay;
		startResources[2] = wood;
		startResources[3] = stone;
		startResources[4] = iron;
		startResources[5] = sulfur;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMapWeather(final Weather set)
	{
		mapWeather = set;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addDiplomacyState(final String name, final int id, final String diplomacy)
	{
		final DiplomacyState diplomacyState = new DiplomacyState(name, id, diplomacy);
		for (int i = 0; i < diplomacyStates.size(); i++)
		{
			if (diplomacyStates.get(i).getPlayer() == id)
			{
				return;
			}
		}
		diplomacyStates.add(diplomacyState);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addPlayerColor(final int player, final String color)
	{
		final PlayerColor playerColor = new PlayerColor(player, color);
		for (int i = 0; i < playerColors.size(); i++)
		{
			if (playerColors.get(i).getPlayer() == player)
			{
				return;
			}
		}
		playerColors.add(playerColor);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PlayerColor> getPlayerColors()
	{
		return playerColors;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DiplomacyState> getDiplomacyStates()
	{
		return diplomacyStates;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Weather getMapWeather()
	{
		return mapWeather;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int[] getStartResources()
	{
		return startResources;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean[] getDebugOptions()
	{
		return debugOptions;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getMetaData()
	{
		return metaData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toJson()
	{
		return String.format(
			"{%s,%s,%s,%s,%s,%s}",
			serializeMeta(),
			serializeDebug(),
			serializeResources(),
			serializeWeather(),
			serializeDiplomacy(),
			serializeColors()
		);
	}

	/**
	 * Serializes the player colors to a json string.
	 * 
	 * @return Json string
	 */
	private Object serializeColors()
	{
		String json = "\"Colors\":  [";
		for (int i = 0; i < playerColors.size(); i++)
		{
			if (i > 0)
			{
				json += ",";
			}
			json += String.format(
				"[%d, \"%s\"]",
				playerColors.get(i).getPlayer(),
				playerColors.get(i).getName()
			);
		}
		json += "]";
		return json;
	}

	/**
	 * Serializes the diplomatic relations to a json string.
	 * 
	 * @return Json string
	 */
	private String serializeDiplomacy()
	{
		String json = "\"Diplomacy\":  [";
		for (int i = 0; i < diplomacyStates.size(); i++)
		{
			if (i > 0)
			{
				json += ",";
			}
			json += String.format(
				"[\"%s\", %d, \"%s\"]",
				diplomacyStates.get(i).getName(),
				diplomacyStates.get(i).getPlayer(),
				diplomacyStates.get(i).getDiplomacy()
			);
		}
		json += "]";
		return json;
	}

	/**
	 * Serializes the weather set to a json string.
	 * 
	 * @return Json string
	 */
	private String serializeWeather()
	{
		return "\"Weather\":\"" + mapWeather.getSet() + "\"";
	}

	/**
	 * Serializes the start resources to a json string.
	 * 
	 * @return Json string
	 */
	private String serializeResources()
	{
		return String.format(
			"\"Resources\":[%d,%d,%d,%d,%d,%d]",
			startResources[0],
			startResources[1],
			startResources[2],
			startResources[3],
			startResources[4],
			startResources[5]
		);
	}

	/**
	 * Serializes the debug configuration to a json string.
	 * 
	 * @return Json string
	 */
	private String serializeDebug()
	{
		return String.format(
			"\"Debug\":[%b,%b,%b,%b]",
			debugOptions[0],
			debugOptions[1],
			debugOptions[2],
			debugOptions[3]
		);
	}

	/**
	 * Serializes the meta data of the map to a json string.
	 * 
	 * @return Json string
	 */
	private String serializeMeta()
	{
		return String.format(
			"\"Meta\":[\"%s\",\"%s\",\"%s\"]",
			metaData[0],
			metaData[1],
			metaData[2]
		);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void fromJson(final JSONObject data)
	{
		// Deserialize meta
		setMapMetaData(
			(String) ((JSONArray) data.get("Meta")).get(0),
			(String) ((JSONArray) data.get("Meta")).get(1),
			(String) ((JSONArray) data.get("Meta")).get(2)
		);

		// Deserialize debug
		setDebugOptions(
			(boolean) ((JSONArray) data.get("Debug")).get(0),
			(boolean) ((JSONArray) data.get("Debug")).get(1),
			(boolean) ((JSONArray) data.get("Debug")).get(2),
			(boolean) ((JSONArray) data.get("Debug")).get(3)
		);

		// Deserialize resources
		setStartResources(
			((Long) ((JSONArray) data.get("Resources")).get(0)).intValue(),
			((Long) ((JSONArray) data.get("Resources")).get(1)).intValue(),
			((Long) ((JSONArray) data.get("Resources")).get(2)).intValue(),
			((Long) ((JSONArray) data.get("Resources")).get(3)).intValue(),
			((Long) ((JSONArray) data.get("Resources")).get(4)).intValue(),
			((Long) ((JSONArray) data.get("Resources")).get(5)).intValue()
		);

		// Deserialize weather
		setMapWeather(new Weather((String) data.get("Weather")));

		// Deserialize diplomacy
		diplomacyStates = new ArrayList<>();
		final List<JSONArray> diplomacy = (JSONArray) data.get("Diplomacy");
		for (int i = 0; i < diplomacy.size(); i++)
		{
			addDiplomacyState(
				(String) diplomacy.get(i).get(0),
				((Long) diplomacy.get(i).get(1)).intValue(),
				(String) diplomacy.get(i).get(2)
			);
		}

		// Deserialize colors
		playerColors = new ArrayList<>();
		final List<JSONArray> colors = (JSONArray) data.get("Colors");
		for (int i = 0; i < colors.size(); i++)
		{
			addPlayerColor(
				((Long) colors.get(i).get(0)).intValue(),
				(String) colors.get(i).get(1)
			);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset()
	{
		playerColors = new ArrayList<>();
		diplomacyStates = new ArrayList<>();
		setStartResources(0,0,0,0,0,0);
		setDebugOptions(false,false,false,false);
		setMapMetaData("","","");
		setMapWeather(new Weather("SetupNormalWeatherGfxSet"));
	}
}
