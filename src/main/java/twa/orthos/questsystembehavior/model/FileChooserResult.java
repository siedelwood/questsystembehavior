package twa.orthos.questsystembehavior.model;

import java.io.File;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileChooserResult
{
    private boolean approved;
    
    private File selectedFile;
}
