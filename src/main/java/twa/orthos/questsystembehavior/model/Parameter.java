
package twa.orthos.questsystembehavior.model;

import java.util.List;

public interface Parameter
{

    String getType();

    String getName();

    List<String> getValues();
}
