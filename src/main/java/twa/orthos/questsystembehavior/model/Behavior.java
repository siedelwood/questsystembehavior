package twa.orthos.questsystembehavior.model;

import java.util.List;

public interface Behavior
{
	String getName();
	
	String getDescription();
	
	List<List<Object>> getFields();
}
