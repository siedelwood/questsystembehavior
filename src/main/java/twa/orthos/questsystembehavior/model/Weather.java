package twa.orthos.questsystembehavior.model;

import org.json.simple.JSONObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Weather
{
	private String set;

	public Weather(final String set)
	{
		this.set = set;
	}
	
	public Weather()
	{
	}
	
	public String toJson()
	{
		return "\"" + set + "\"";
	}
	
	public static Weather toObject(final JSONObject data)
	{
		final Weather object = new Weather();
		object.setSet((String) data.get("Set"));
		return object;
	}
}
