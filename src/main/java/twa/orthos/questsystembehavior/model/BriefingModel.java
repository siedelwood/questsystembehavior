package twa.orthos.questsystembehavior.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BriefingModel implements Briefing
{
	protected List<Page> pages;
	
	protected String name;
	
	public BriefingModel()
	{
		pages = new ArrayList<>();
	}
	
	public BriefingModel(final String name)
	{
		this.name = name;
		pages = new ArrayList<>();
	}

	@Override
	public Page getPage(final int index)
	{
		if ((pages.size()-1) < index) {
			return null;
		}
		return pages.get(index);
	}

	@Override
	public void addPage(final Page page)
	{
		pages.add(page);
	}

    @Override
    public void addPage(final Page page, final int index)
    {
        pages.set(index, page);
    }

	@Override
	public void removePage(final int index)
	{
		pages.remove(index);
	}
}
