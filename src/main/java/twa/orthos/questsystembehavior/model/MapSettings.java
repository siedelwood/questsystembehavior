
package twa.orthos.questsystembehavior.model;

import java.util.List;

import org.json.simple.JSONObject;

public interface MapSettings
{

    void setMapMetaData(String name, String author, String version);

    void setDebugOptions(boolean check, boolean cheats, boolean shell, boolean trace);

    void setStartResources(int gold, int clay, int wood, int stone, int iron, int sulfur);

    void setMapWeather(Weather weather);

    void addDiplomacyState(String name, int id, String diplomacy);

    void addPlayerColor(int player, String color);

    List<PlayerColor> getPlayerColors();

    Weather getMapWeather();

    int[] getStartResources();

    boolean[] getDebugOptions();

    String[] getMetaData();

    List<DiplomacyState> getDiplomacyStates();

    String toJson();

    void fromJson(final JSONObject data);

    void reset();
}
