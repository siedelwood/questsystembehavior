
package twa.orthos.questsystembehavior.model;

import java.util.List;

public interface Quest
{

    String getName();

    String getTime();

    String getReceiver();

    String getQuestTitle();

    String getQuestText();

    String getQuestType();

    void setName(String text);

    void setTime(String text);

    void setReceiver(String text);

    void setQuestTitle(String text);

    void setQuestText(String text);

    void setQuestType(String text);

    List<List<String>> getBehavior();

    List<String> getSingleBehavior(final int index);

    int countBehavior();

    boolean isVisible();

    void setVisible(boolean visible);
}
