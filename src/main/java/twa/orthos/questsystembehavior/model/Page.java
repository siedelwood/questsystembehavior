
package twa.orthos.questsystembehavior.model;

public interface Page
{

    String getTitle();

    String getText();

    String getPosition();

    String getAction();

    boolean isDialog();

    boolean isSkyShown();

    boolean isFogDisabled();

    void setTitle(String title);

    void setText(String text);

    void setPosition(String position);

    void setAction(String action);

    void setDialog(boolean flag);

    void setSkyShown(boolean flag);

    void setFogDisabled(boolean flag);
}
