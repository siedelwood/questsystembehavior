package twa.orthos.questsystembehavior.model;

import java.util.List;

public interface Briefing
{
	String getName();
	
	void setName(String name);
	
	List<Page> getPages();
	
	void setPages(List<Page> pages);
	
	Page getPage(int index);
	
	void addPage(Page page);
    
    void addPage(Page page, int index);
	
	void removePage(int index);
}
