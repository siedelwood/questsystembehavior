package twa.orthos.questsystembehavior.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BehaviorModel implements Behavior
{
	protected String name;
	
	protected String description;
	
	protected List<List<Object>> fields;
}
