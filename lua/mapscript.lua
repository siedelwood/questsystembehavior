-- Include globals
Script.Load("data/script/maptools/main.lua");
Script.Load("data/script/maptools/mapeditortools.lua");

-- Load library
Script.Load("data/maps/externalMap/qsb/oop.lua");
Script.Load("data/maps/externalMap/qsb/questsystem.lua");
Script.Load("data/maps/externalMap/qsb/questdebug.lua");
Script.Load("data/maps/externalMap/qsb/interaction.lua");
Script.Load("data/maps/externalMap/qsb/information.lua");
Script.Load("data/maps/externalMap/qsb/questbehavior.lua");

-- Include main mapscript
Script.Load("data/maps/externalMap/mainmapscript.lua");

--
-- Füge in dieser Funktion deine eigenen Inhalte hinzu. Diese Funktion wird
-- aufgerufen, sobald alle Voreinstellungen gemacht wurden.
--
function PreperationDone()

end

-- Platz für deine Funktionen...

