-- ########################################################################## --
-- #  Internal main mapscript                                               # --
-- #  --------------------------------------------------------------------  # --
-- #    Author:   totalwarANGEL                                             # --
-- ########################################################################## --

--
-- Setzt die im Assistenten eingestellten diplomatischen Beziehungen für
-- den menschlichen Spieler. Außerdem werden die Namen der Parteien gesetzt.
--
function InitDiplomacy()
    for k, v in pairs(QUEST_ASSISTENT_SETTINGS_DIPLOMACY) do
        if k ~= gvMission.PlayerID then
            Logic.SetDiplomacyState(gvMission.PlayerID, k, v[2])
        end
        if v[1] ~= "" then
            SetPlayerName(k, v[1])
        end
    end
end

--
-- Gibt dem menschlichen Spieler die eingestellten Startrohstoffe.
--
function InitResources()
    Tools.GiveResouces(gvMission.PlayerID, unpack(QUEST_ASSISTENT_SETTINGS_RESOURCES))
end

--
-- Verbietet alle im Assistenten eingestellten Technologien.
--
function InitTechnologies()
end

--
-- Ruft das im Assistenten eingestellte Wetterset auf.
--
function InitWeatherGfxSets()
	QUEST_ASSISTENT_SETTINGS_WEATHER_SET
end

--
-- Setzt die Spielerfarben der Parteien fest.
--
function InitPlayerColorMapping()
    for k, v in pairs(QUEST_ASSISTENT_SETTINGS_PLAYER_COLOR) do
        if v then
            Display.SetPlayerColorMapping(k, v)
        end
    end
end

--
-- Startet alle Prozesse, die zu Beginn der Mission aktiviert werden müssen.
--
function FirstMapAction()
    Score.Player[0] = {}
	Score.Player[0]["buildings"] = 0
	Score.Player[0]["all"] = 0
    
    QuestSystemBehavior:PrepareQuestSystem();
    local DebugConfig = QUEST_ASSISTENT_SETTINGS_DEBUG_OPTIONS
    if DebugConfig[1] or DebugConfig[2] or DebugConfig[3] or DebugConfig[4] then
        QuestSystemDebug:Activate(unpack(DebugConfig))
    end
    CreateQuests()
    PreperationDone()
end

--
-- Hier werden die im Assistenten angelegten Quests definiert und gestartet.
--
function CreateQuests()
    local QuestsToGenerate = QUEST_ASSISTENT_GENERATED_QUESTS
    for k, v in pairs(QuestsToGenerate) do
        local Behaviors = {}
        for i= 1, table.getn(v), 1 do
            local CurrentBehaviorData = copy(v[i])
            local CurrentBehaviorName = table.remove(CurrentBehaviorData, 1)
            table.insert(Behaviors, _G[CurrentBehaviorName](unpack(CurrentBehaviorData)))
        end

        CreateQuest {
            Name        = v.Name,
            Description = v.Description,
            Receiver    = v.Receiver,
            Time        = v.Time,

            unpack(Behaviors)
        }
    end
end

-- -------------------------------------------------------------------------- --

QUEST_ASSISTENT_GENERATED_BRIEFINGS
